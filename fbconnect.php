<?php
/**
* Copyright 2011 Facebook, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may
* not use this file except in compliance with the License. You may obtain
* a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/
include ('../../../wp-load.php');

 require('classes/facebook/facebook.php'); 
// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(
  'appId' => get_option( VWT_PREFIX .'fb_app_id' ),
  'secret' => get_option( VWT_PREFIX .'fb_app_secret' ),
));

// Get User ID
$user = $facebook->getUser();

// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

if ($user) {
  try {
    // Proceed knowing you have a logged in user who's authenticated.
    $user_profile = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }
}

// Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl = $facebook->getLogoutUrl();
} else {
  $statusUrl = $facebook->getLoginStatusUrl();
  $loginUrl = $facebook->getLoginUrl();
}

$pid = $_GET['pid'];  // Get the post id of the referring post.
$email_list = $_GET['list'];
$redirect_url = $_GET['url'];

$meta_data = get_post_meta($pid);  // Get the post meta data of the referring post.
$contest = $meta_data[VWT_PREFIX .'lo_contest'][0];  // Is there a contest?
$total_conversions = $meta_data[VWT_PREFIX . 'lo_number_of_offers'][0]; // Total number of offers available.
$offers_received = $meta_data[VWT_PREFIX .'lo_offers_recieved'][0];  // Total number of offers redemed.

$remaining_conversions = ($total_conversions - $offers_received);


global $wpdb;

$uid = ($wpdb->get_row("SELECT ID FROM wp_posts WHERE post_title = '$user_profile[email]'", 'ARRAY_A')) ; // Find Post ID  Checks to see if subscriber already exists.
if ($uid) {
	$uid = array_values($uid);
	$uid = $uid[0];
	
	$list_id = ($wpdb->get_row("SELECT term_id FROM wp_terms WHERE name = '$email_list'", 'ARRAY_A')); //Find Email List ID.
	$list_id = array_values($list_id);
	$list_id = $list_id[0];
	//$list_id = '6';
}
// If the subscriber already exists on the list then we'll just go ahead and redirect them.
if ($wpdb->get_results("SELECT * FROM wp_term_relationships WHERE object_id = '$uid' AND term_taxonomy_id = '$list_id' ", 'ARRAY_A')) { 
	  header("Location: $redirect_url");
  } 
// The user is either new or new to the list so we'll add them to the new list and redirect.
else {
	  mymail_subscribe( $user_profile[email], $userdata = array(
		'firstname' => $user_profile[first_name], 
		'lastname' => $user_profile[last_name], 
		'facebook-profile-link' => $user_profile[link], 
		'facebook-username' => $user_profile[username],
		'facebook-id' => $user_profile[id],
		'current-city' => $user_profile[location][name],
		'birthday' => $user_profile[birthday]
		), $lists = array($email_list), $double_opt_in = NULL, $overwrite = true, $mergelists = NULL, $template = 'notification.html' );

  // Check to see if we are running a contest.
  if ($contest == 'on') {
	  // The subscriber has been added to the list so lets go ahead and update the count.
	  if (empty($offers_received)) {
		  update_post_meta($pid, VWT_PREFIX .'lo_offers_recieved', 1);
		  } 
	  else {
		  update_post_meta($pid, VWT_PREFIX .'lo_offers_recieved', ++$offers_received);
		}
	}
}

// New subscriber has been added to the list so let's redirect them.  
header("Location: $redirect_url");
?>