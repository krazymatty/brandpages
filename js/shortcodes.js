function facebookform() {
    return '[facebookform url="https://your-register-form-url.com" width="520"/]';
}
 
(function() {
    tinymce.create('tinymce.plugins.facebookform', {
        init : function(ed, url){
			// Register button
            ed.addButton('facebookform', {
            title : 'Insert Facebook Registration Form',
                onclick : function() {
                    ed.execCommand(
                    'mceInsertContent',
                    false,
                    facebookform()
                    );
                },
                image: url + "/form-icon.png"
			});

		},

	});
    tinymce.PluginManager.add('facebookform', tinymce.plugins.facebookform);
})();

function facebooklogin() {
    return '[facebooklogin button_url="https://your-image-url.jpg"/]';
}
 
(function() {
    tinymce.create('tinymce.plugins.facebooklogin', {
        init : function(ed, url){
			// Login button
            ed.addButton('facebooklogin', {
            title : 'Insert Facebook Login Button',
                onclick : function() {
                    ed.execCommand(
                    'mceInsertContent',
                    false,
                    facebooklogin()
                    );
                },
                image: url + "/login-icon.png"
			});

		},

	});
    tinymce.PluginManager.add('facebooklogin', tinymce.plugins.facebooklogin);
})();

function lo_contest() {
    return '[lo_contest button_url="https://your-image-url.jpg" title="The Title" bottom_message="Remaining"]Details (optional)[/lo_contest]';
}
 
(function() {
    tinymce.create('tinymce.plugins.lo_contest', {
        init : function(ed, url){
			// Login button
            ed.addButton('lo_contest', {
            title : 'Insert Contest Button',
                onclick : function() {
                    ed.execCommand(
                    'mceInsertContent',
                    false,
                    lo_contest()
                    );
                },
                image: url + "/login-icon.png"
			});

		},

	});
    tinymce.PluginManager.add('lo_contest', tinymce.plugins.lo_contest);
})();

function facebookcomments() {
    return '[facebookcomments posts="2" width="520"/]';
}
 
(function() {
    tinymce.create('tinymce.plugins.facebookcomments', {
        init : function(ed, url){
			// Register button
            ed.addButton('facebookcomments', {
            title : 'Insert Facebook Comments',
                onclick : function() {
                    ed.execCommand(
                    'mceInsertContent',
                    false,
                    facebookcomments()
                    );
                },
                image: url + "/comment-icon.png"
			});

		},

	});
    tinymce.PluginManager.add('facebookcomments', tinymce.plugins.facebookcomments);
})();

/*(function() {
	tinymce.create('tinymce.plugins.ShortcodeExecPHP', {
		init: function(ed, url) {
			// Register command
			ed.addCommand('mceShortcodeExecPHP', function() {
				ed.windowManager.open({
					file: ajaxurl + '?action=scep_ajax&scep_action=tinymce',
					width: 320 + ed.getLang('ShortcodeExecPHP.delta_width', 0),
					height: 240 + ed.getLang('ShortcodeExecPHP.delta_height', 0),
					inline: 1
				}, {
					plugin_url: url // Plugin absolute URL
				});
			});

			// Register button
			ed.addButton('ShortcodeExecPHP', {
				title: 'Shortcode',
				cmd: 'mceShortcodeExecPHP',
				image: url + '/shortcode.gif'
			});

			// Add a node change handler, selects the button in the UI when a image is selected
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('ShortcodeExecPHP', n.nodeName == 'IMG');
			});
		},

		createControl: function(n, cm) {
			return null;
		},

		getInfo: function() {
			return {
				longname : 'Shortcode Exec PHP plugin',
				author : 'Marcel Bokhorst',
				authorurl : 'http://blog.bokhorst.biz/about/',
				infourl : 'http://blog.bokhorst.biz/3626/computers-en-internet/wordpress-plugin-shortcode-exec-php/',
				version : '1.0'
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('ShortcodeExecPHP', tinymce.plugins.ShortcodeExecPHP);
})();
*/