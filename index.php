<?php
// Include constants file
require_once( dirname( __FILE__ ) . '/lib/constants.php' );

class DIYBrandPages {
    var $namespace = "diy-brand-pages";
    var $friendly_name = "DIY Brand Pages";
    var $version = "1.0.0";
    
    // Default plugin options
    var $defaults = array (
				array( "name" => ($this->name = "name"),
					"type" => "title"),

				array( "name" => "Activation",
					"type" => "section"),
				array( "type" => "open"),
				array( "name" => "License Key",
					"desc" => "Enter your License Key",
					"id" => DIY_PREFIX."activation_key",
					"type" => "text",
					"std" => ""),
				array( "name" => "Email",
					"desc" => "Enter the email you used when you registerd your ".DIY_PLUGIN_NAME." Plugin ",
					"id" => DIY_PREFIX."registered_email",
					"type" => "text",
					"std" => ""),
				array( "type" => "close"),

				array( "name" => "Facebook Application Setup",
					"desc" => "Enter Your Facebook App Id & Secret",
					"type" => "section"),
				array( "type" => "open"),
				array( "name" => "Facebook User ID",
					"desc" => "Enter Your facebook User ID.  Find It Here: https://graph.facebook.com/YourUserName",
					"id" => DIY_PREFIX."fb_user_id",
					"type" => "text",
					"std" => ""),
				array( "name" => "Facebook App ID",
					"desc" => "Enter Your facebook Application ID",
					"id" => DIY_PREFIX."fb_app_id",
					"type" => "text",
					"std" => ""),
				array( "name" => "Facebook App Secret",
					"desc" => "Enter Your facebook Application Secret",
					"id" => DIY_PREFIX."fb_app_secret",
					"type" => "text",
					"std" => ""),
				array( "type" => "close"),

				array( "name" => "Plugin Setup",
					"desc" => "Change Plugin Defaults",
					"type" => "section"),
				array( "type" => "open"),
				array( "name" => "Fanpage Directory",
					"desc" => "Change your Fanpages directory.<br/>Current: ".DIY_SITE_URL."/<span style='color:black;'>".FANPAGE_DIR."/</span><br/><span style='color:red;'><em>You MUST Update <a href='/wp-admin/options-permalink.php'>Permalinks!</a></span></em>",
					"id" => DIY_PREFIX."fanpage_dir",
					"type" => "text",
					"std" => ""),
				array( "name" => "FB Register Directory",
					"desc" => "Change your FB-Register directory.<br/>Current: ".DIY_SITE_URL."/<span style='color:black;'>".FANPAGE_DIR_REGISTER."/</span><br/><span style='color:red;'><em>You MUST Update <a href='/wp-admin/options-permalink.php'>Permalinks!</a></span></em>",
					"id" => DIY_PREFIX."fanpage_dir_register",
					"type" => "text",
					"std" => ""),
				array( "name" => "Redirect Link Directory",
					"desc" => "Change you Redirect Directory.<br/>Current: ".DIY_SITE_URL."/<span style='color:black;'>".FANPAGE_DIR_REDIRECT."/</span><br/><span style='color:red;'><em>You MUST Update <a href='/wp-admin/options-permalink.php'>Permalinks!</a></span></em>",
					"id" => DIY_PREFIX."fanpage_dir_redirect",
					"type" => "text",
					"std" => ""),
				array( "name" => "Fanpage Url",
					"desc" => "Enter Your facebook fanpage url.",
					"id" => DIY_PREFIX."fanpage_url",
					"type" => "text",
					"std" => ""),
				array( "name" => "Fanpage Width",
					"desc" => "Enter Facebook Page Width. 520 or 810",
					"id" => DIY_PREFIX."fanpage_width",
					"type" => "text",
					"std" => "810"),
				array( "type" => "close"),

				array( "name" => "Make Money with ".DIY_PLUGIN_NAME."",
					"type" => "section"),
				array( "type" => "open"),
				array( "name" => "Affiliate ID",
					"desc" => "Enter Your Affiliate ID from the members area.",
					"id" => DIY_PREFIX."affiliate_id",
					"type" => "text",
					"std" => ""),
				array( "type" => "close")
);


    /**
     * Instantiation construction
     * 
     * @uses add_action()
     * @uses DIYBrandPages::wp_register_scripts()
     * @uses DIYBrandPages::wp_register_styles()
     */
    function __construct() {
        // Name of the option_value to store plugin options in
        $this->option_name = '_' . $this->namespace . '--options';
		
        // Load all library files used by this plugin
        $libs = glob( DIYBRANDPAGES_DIRNAME . '/lib/*.php' );
        foreach( $libs as $lib ) {
            include_once( $lib );
        }
        
        /**
         * Make this plugin available for translation.
         * Translations can be added to the /languages/ directory.
         */
        load_theme_textdomain( $this->namespace, DIYBRANDPAGES_DIRNAME . '/languages' );

		// Add all action, filter and shortcode hooks
		$this->_add_hooks();
    }
    /**
     * Add in various hooks
     * 
     * Place all add_action, add_filter, add_shortcode hook-ins here
     */
    private function _add_hooks() {
				add_action('admin_notices', 'diy_admin_notice_needs_activated',10,'');
				add_action('admin_menu', 'add_diy_settingsMenu');

        // Options page for configuration
        add_action( 'admin_menu', array( &$this, 'admin_menu' ) );
        // Route requests for form processing
        add_action( 'init', array( &$this, 'route' ) );
        
        // Add a settings link next to the "Deactivate" link on the plugin listing page
        add_filter( 'plugin_action_links', array( &$this, 'plugin_action_links' ), 10, 2 );
        
        // Register all JavaScripts for this plugin
        add_action( 'init', array( &$this, 'wp_register_scripts' ), 1 );
        // Register all Stylesheets for this plugin
        add_action( 'init', array( &$this, 'wp_register_styles' ), 1 );
    }

    /**
     * Initialization function to hook into the WordPress init action
     * 
     * Instantiates the class on a global variable and sets the class, actions
     * etc. up for use.
     */
    static function instance() {
        global $DIYBrandPages;
        
        // Only instantiate the Class if it hasn't been already
        if( !isset( $DIYBrandPages ) ) $DIYBrandPages = new DIYBrandPages();
    }
	



}
if( !isset( $DIYBrandPages ) ) {
	DIYBrandPages::instance();
}

register_activation_hook( __FILE__, array( 'DIYBrandPages', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'DIYBrandPages', 'deactivate' ) );

?>