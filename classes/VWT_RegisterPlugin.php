<?php 
//require_once('VWT_PluginStatus.php');

class VWT_RegisterPlugin {
		
	public function checkactivation($url) {
		global $wpdb;
		
		if ($_POST[VWT_PREFIX . 'activation_key']){
			$activation_key = $_POST[VWT_PREFIX . 'activation_key'];
		}
		else {
			$activation_key = get_option(VWT_PREFIX . 'activation_key');
		}
		if (!$activation_key) { define('VWT_PSTATUS', 'no-key');
		}
		if ($_POST[VWT_PREFIX . 'registered_email']){
			$registered_email = $_POST[VWT_PREFIX . 'registered_email'];
		}
		else {
			$registered_email = get_option(VWT_PREFIX . 'registered_email');
		}
		
		$check_date = get_option(VWT_PREFIX . 'check_date');
		$curdate = date("m-d-y",time());
		
		if ($_POST[VWT_PREFIX . 'activation_key'] || ($activation_key && $check_date != $curdate)){
			$data="key=$activation_key&email=$registered_email";
			$post_url = $url;
			$curl_handle = curl_init();
			curl_setopt($curl_handle, CURLOPT_URL, $post_url);
			curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_POST, 1);
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
			$plugin_status = curl_exec($curl_handle);
			curl_close($curl_handle);
			parse_str($plugin_status);
			
			if(!$plugin_status){
				$plugin_status = file_get_contents($post_url.'?'.$data);
			}
			if($status == 'Standard' OR $status == 'Developer'){
				update_option( VWT_PREFIX . 'status', $status );
				update_option( VWT_PREFIX . 'check_date', $curdate );
			}

			if( !defined( 'VWT_PSTATUS' ) ) define('VWT_PSTATUS', $plugin_status);
			if( !defined( 'VWT_STATUS' ) ) define('VWT_STATUS', $status);
			if( !defined( 'VWT_VALID_URL' ) ) define('VWT_VALID_URL', $valid_url);
			if( !defined( 'VWT_VALID_KEY' ) ) define('VWT_VALID_KEY', $valid_key);
		}		
	}
}
?>