<?php 
class Dynamic_Meta_Box {
	function __construct($name, $title, $prefix, $posttype) {
			$this->name		= strtolower( str_replace( ' ', '_', $name ) );
			$this->prefix		= $prefix;
			$this->posttype		= $posttype;
			$this->title		= $title;
			add_action( 'add_meta_boxes', array(&$this, 'add_custom_box' ));
			add_action( 'save_post', array(&$this, 'save_postdata' ));

		}

		/* Adds a box to the main column on the Post and Page edit screens */
		function add_custom_box() {
				add_meta_box(
						$this->name,
						__( $this->title, $this->prefix.'meta_box' ),
						array(&$this, 'custom_box'),
						$this->posttype);
		}
		/* Prints the box content */
		function custom_box() {
			global $post;
			// Use nonce for verification
			wp_nonce_field( plugin_basename( __FILE__ ), $this->prefix.'Meta_noncename' );
		
			?>
				<style type="text/css">
				<?php include 'meta-box/css/meta-css.css'; ?>
				</style>
				<div id="<?php echo  $this->prefix ?>container" class="meta_inner">
				<?php
				echo '
				<ol id="'.$this->prefix.'formFields-meta" class="form-field-desc">
					<li id="'.$this->prefix.'headtab" class="form-field-desc-row">
					<div id="'.$this->prefix.'meta-form_name" class="meta-form-name">Form Name</div>  
					<div id="'.$this->prefix.'meta-form_value" class="meta-form-value">Form Value</div> 
					</li>
				';
			//get the saved meta as an arry
			$formFields = get_post_meta($post->ID,$this->prefix.'formFields',true);
			$c = 0;
			if(is_array($formFields)){
					foreach($formFields as $field_name ){
						if (isset($field_name['form_name']) || isset($field_name['form_value']) ){
							echo '
							<li>Form Name: 
							<input type="text" id="'.$this->prefix.'meta-form_name" class="meta-form-name" name="'.$this->prefix.'formFields['.$c.'][form_name]" value="'.$field_name['form_name'].'" />  
							Form Value: <input type="text" id="'.$this->prefix.'meta-form_value" class="meta-form-value" name="'.$this->prefix.'formFields['.$c.'][form_value]" value="'.$field_name['form_value'].'" />  
							
							<span class="remove" title="Delete">Remove</span>
							</li>';
							$c = $c +1;
							}
					}
			}
			echo '</ol>';
			?>
			<span id="<?php echo  $this->prefix ?>here" class="here" ></span>
			<span id="<?php echo  $this->prefix ?>add" class="add"><?php echo __('Add Form Field/Name'); ?></span>
			<script>
					var $ =jQuery.noConflict();
					$(document).ready(function() {
							var count = <?php echo $c; ?>;
							$("#<?php echo  $this->prefix ?>add").click(function() {
									count = count + 1;
									$('#<?php echo  $this->prefix ?>formFields-meta').append('<li>Form Name: <input type="text" id="<?php echo  $this->prefix ?>meta-form_name" class="meta-form-name" name="<?php echo  $this->prefix ?>formFields['+count+'][form_name]" value="" /> Form Value: <input type="text" id="<?php echo  $this->prefix ?>meta-form_value" class="meta-form-value" name="<?php echo  $this->prefix ?>formFields['+count+'][form_value]" value="" /> <span class="remove" title="Delete">Remove</span> </li> ' );
									return false;
							});
							$(".remove").live('click', function() {
									$(this).parent().remove();
							});
					});
			</script>
			</div>
			<?php
    }
		/* When the post is saved, saves our custom data */
		function save_postdata( $post_id ) {
				// verify if this is an auto save routine. 
				// If it is our form has not been submitted, so we dont want to do anything
				if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
						return;
				// verify this came from the our screen and with proper authorization,
				// because save_post can be triggered at other times
				if (isset($_POST[$this->prefix.'Meta_noncename'])){
						if ( !wp_verify_nonce( $_POST[$this->prefix.'Meta_noncename'], plugin_basename( __FILE__ ) ) )
								return;
				}else{return;}
				// OK, we're authenticated: we need to find and save the data
				$formFields = $_POST[$this->prefix.'formFields'];
				update_post_meta($post_id,$this->prefix.'formFields',$formFields);
		}
}

?>