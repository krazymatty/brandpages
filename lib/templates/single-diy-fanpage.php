<?php
/**
Template Name: DIY Fanpage
**/

//$_SERVER['SERVER_NAME'];
//$data = array();
$rand = rand();
wp_deregister_script('jquery');
wp_enqueue_style('fanpage-css', VWT_PLUGINURL ."css/fanpage.css?$rand", false, '1.0', 'all'); 
wp_enqueue_style('countdown-css', VWT_PLUGINURL ."css/jcountdown-1.1.css", false, '1.0', 'all'); 
wp_enqueue_script('jquery',  VWT_PLUGINURL . "js/jquery-1.8.2.min.js", false, '1.0'); 
wp_enqueue_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js", false, '1.0'); 
//wp_enqueue_script('countdown-js', VWT_PLUGINURL . "js/jquery.jcountdown-1.1.js", false, '1.0'); 
wp_enqueue_script('countdown-js', VWT_PLUGINURL . "js/jquery.jcountdown.min.js", false, '1.0'); 

require_once('inc/meta_data.inc.php');
require_once('inc/signed_request.inc.php');


$site_url = get_bloginfo('url');
$fanpage_url = get_option(VWT_PREFIX . 'fanpage_url'); 
$post_fanpage_url = $meta_data[VWT_PREFIX . 'fanpage_url'][0];
$timer = $meta_data[VWT_PREFIX . 'countdown_timer'][0];
$timer = date('Y/m/d H:i:s', $timer);
//var_dump($meta_data);
//exit;

// If sombody visits this page out side of facebook we are going to redirect to the fan page.
/*if (!$signed_request) { 
// Check to see if a the url to this page is set and redirect to it.
  if ($post_fanpage_url) {
	  header("Location: $post_fanpage_url "); exit; } 
// If the page url is not set then let's redirect to the main fanpage url set in the setup tab. 
  if (!$fanpage_url) { $fanpage_url = get_bloginfo('url');  
	  header("Location: $fanpage_url"); exit; } 
  else {
	  header("Location: $fanpage_url"); exit; } 
}*/
// Any time we want to redirect our fanpage we add a link to app_data.  ?app_data=link,http:example.com&
if ($app_data){
  if (strpos($app_data, 'link') === false ){ } else {
		$fb_link = preg_split("/,/", $app_data);
		header("Location: $fb_link[1]"); 
		exit; 
	}

	// This is the response from the upadated domains form submission.
	if ('script-call-success' == $app_data){
		$go = $site_url.'/activation-key/?status=successful&msg=Your%20domains%20were%20successfully%20saved!';
		 echo header("Location: $go");
		 exit;
	} 
		
	if ('script-call-fail' == $app_data){
		$go = $site_url.'/activation-key/?status=failed&msg=ERROR%3A%20Your%20information%20failed%20to%20update.';
		 echo header("Location: $go");
		 exit; 
	}
 
} 


//echo $app_data;
//exit;
// If this is a a reveal page that has already been liked and we are asked to redirect then we redirect.
if($reveal && ($like_status) && ($redirect)){
	echo header("Location: $redirect"); 
	exit;
}
// Template Setup.
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo the_title();?></title>
<?php if ($fanpage_title) { echo '<meta property="og:title" content="'.$fanpage_title.'" />'; } ?>
<?php if ($fanpage_desc) { echo '<meta property="og:description" content="'.$fanpage_desc.'" />'; } ?>
<?php if ($fb_images) { foreach ($fb_images as &$value) {
echo '<meta property="og:image" content="'.$value.'" />';
} }?>
<meta property="fb:admins" content="<?php echo $fb_user_id ;?>" />
<meta property="fb:app_id" content="<?php echo $app_id ;?>" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?<?php echo rand() . "";?>" type="text/css" />
<?php wp_head(); ?>
<style type="text/css">
<?php echo $header_image.''.$header_height.' '.$css.' iframe.fb_ltr { height:30px; }';  ?>
</style>
<!--<script src="https://connect.facebook.net/en_US/all.js#appId=<?php echo $app_id ;?>&xfbml=1"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery.ajaxSetup({ cache: true });
  jQuery.getScript('//connect.facebook.net/en_UK/all.js', function(){
    FB.init({
      appId: '<?php echo $app_id ;?>',
      channelUrl: '<?php echo VWT_DIRURL ?>inc/channel.html',
    });     
	  FB.Canvas.setAutoGrow();
  });  
});
</script>-->

<?php if ($header_shadow) {
	$shadow = "shadow";
	$hshadow = "header-shadow"; 
} else {
	$shadow = "no-shadow";
	$hshadow = "no-header-shadow"; 
	}
?>
</head>

<body class="<?php echo $style; ?>">
<div id="diy-wrap">
  <?php if ($menu_above) { 
	  $defaults = array(
	'theme_location'  => '',
	'menu'            => '', 
	'container'       => 'div', 
	'container_class' => 'menu-fanpage-container', 
	'container_id'    => '',
	'menu_class'      => 'menu', 
	'menu_id'         => 'fanpage-menu',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
); 
		wp_nav_menu( $defaults ); } ?>
  <div id="diy-header" class="<?php echo $hshadow ?>"></div>
  <?php if ($menu_below) { 
	  wp_nav_menu( array( 
	  'container' => 'div',
	  'container_class' => 'menu-fanpage-container',
	  'menu_class' => 'menu',
	  'menu_id' => 'fanpage-menu',
	  'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
	  )
	   ); } ?>
  <?php
	if ($sidebar_layout == 'R') { ?>
  <div id="diy-main" class="sidebar_r <?php echo $shadow ?>">
    <div id="diy-content">
      <?php
        if($reveal && ($like_status)) { 
          echo do_shortcode( $content ); }
        else {
          if (have_posts()) : while (have_posts()) : the_post();    
            the_content();
          endwhile;
          endif; }
        ?>
    </div>
    <div id="diy-sidebar" class="right">
      <?php dynamic_sidebar( 'diy-sidebar' ); ?>
    </div>
    <div style="clear:both;"></div>
  </div>
  <?php } 
		
      if ($sidebar_layout == 'L') { ?>
  <div id="main" class="sidebar_l<?php echo $shadow ?>">
    <div id="diy-sidebar" class="left">
      <?php dynamic_sidebar( 'diy-sidebar' ); ?>
    </div>
    <div id="diy-content">
      <?php
          if($reveal && ($like_status)) { 
            echo do_shortcode( $content ); }
          else {
            if (have_posts()) : while (have_posts()) : the_post();    
              the_content();
            endwhile;
            endif; }
          ?>
    </div>
    <div style="clear:both;"></div>
  </div>
  <?php } 
	  
	  if ($sidebar_layout == 'No') { ?>
  <div id="diy-main" class="<?php echo $shadow ?>" >
    <div id="diy-content">

      <?php
          if($reveal && ($like_status)) { 
            echo do_shortcode( $content ); }
          else {
            if (have_posts()) : while (have_posts()) : the_post();    
              the_content();
            endwhile;
            endif; }
          ?>
    </div>
    <div style="clear:both;"></div>
  </div>
  <?php } ?>
  <?php if (!$disable_credit){
		?><div id="fb-credit" style="clear:both; text-align:right; margin-right:10px;">Powerd By: <a href="<?php $affiliate_id = get_option(VWT_PREFIX . 'affiliate_id'); if ($affiliate_id) { echo 'https://diybrandpages.com/dap/a/?a='.$affiliate_id.'=https://diybrandpages.com/custom-fanpages/';} else {echo 'https://diybrandpages.com/custom-fanpages/';}; ?>" style="color: #900; text-decoration:none;">DIY Brand Pages</a> </div>
    <?php
		};?>
</div>
<?php wp_footer(); ?>

<script>
	jQuery(document).ready(function(){
	  jQuery("body").html(jQuery("body").html().replace('{COUNTDOWN_TIMER_CONTENT}','<div id="timer-content" class="hide"><?php echo $timer_content; ?></div>').replace('{COUNTDOWN_TIMER}','<div id="timer"></div>'));
	  //jQuery("#timer-content").hide();
	  jQuery("#timer").jCountdown({
			timeText:"<?php echo $timer; ?>",
			timeZone:<?php echo get_option('gmt_offset'); ?>,
			style:"slide",
			color:"black",
			width:0,
			textGroupSpace:15,
			textSpace:0,
			reflection:true,
			reflectionOpacity:10,
			reflectionBlur:0,
			dayTextNumber:2,
			displayDay:true,
			displayHour:true,
			displayMinute:true,
			displaySecond:true,
			displayLabel:true,
			onFinish:function(){
				<?php if ($timer_redirect) { 
				echo "window.location = \"$timer_redirect\""; } ?> 
				jQuery("#timer-content").removeClass('hide');
				jQuery("#timer").hide();
			}
		});
	});
</script>
</html>