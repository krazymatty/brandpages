<?php
/**

Template Name: DIY-Link

**/

// Get Post Meta Data
require_once('inc/meta_data.inc.php');

/*  SET OG:TYPE
************************************************************/
if ($og_type == 'music') { $og_type = 'music.song'; }

// Check to see if we are redirecting to a fanpage
if ($sendto_fanpage_url) {
// if we are redirecting to a specific page on the fanpage
	if ($link_url) {
	  $url = $post_fanpage_url.'&app_data=link,'.$link_url; } 
// otherwise we redirect to the fanpage
	else { $url = $post_fanpage_url; }
}
// not a fanpage so nothing special
else { $url = $post_fanpage_url; }

?>

<?php echo '<meta property="fb:app_id" content="'.$fb_app_id.'" />' . "\n";?>
<?php //echo '<meta property="fb:admins" content="'.$fb_user_id.'" />';?>

<?php if ($fanpage_title) { echo '<meta property="og:title" content="'.$fanpage_title.'"/>' . "\n"; } ?>
<?php if ($fanpage_desc) { echo '<meta property="og:description" content="'.$fanpage_desc.'"/>' . "\n"; } ?>
<?php echo '<meta property="og:site_name" content="'.get_bloginfo('name').'"/>' . "\n"; ?>
<?php if ($fb_images) { foreach ($fb_images as &$value) { echo '<meta property="og:image" content="'.$value.'"/>' . "\n"; } }?>
<?php echo '<meta property="og:url" content="'.$permalink.'" />' . "\n";?>
<?php if ($og_type == 'video') { 
	$og_type = 'video.other'; ?>
<?php echo '<meta property="og:type" content="'.$og_type.'"/>' . "\n";?>
<?php
	echo '<meta property="og:video" content="'.$video_url.'"/>' . "\n";
  echo '<meta property="og:video:width" content="'.$video_width.'" /> ' . "\n";
  echo '<meta property="og:video:height" content="'.$video_height.'" />' . "\n";
  echo '<meta property="og:video:type" content="application/x-shockwave-flash" />' . "\n";
 } ?>


<?php //exit; ?>
<?php
print '<script type="text/javascript">
<!--
window.location = "'.$url.'";
//-->
</script>';
?>