<?php
/**

Template Name: CF Register

**/

// REQUIREMENTS
require_once('inc/options.inc.php');
require_once('inc/meta_data.inc.php');
//require_once('inc/form_fields.inc.php');
require_once('inc/signed_request.inc.php');
require_once('inc/mailchimp.inc.php');
require_once('inc/mymail.inc.php');
require_once('inc/post_variables.inc.php');

// IF WE HAVE TWO FORMS THEN WE ARE SENDING MULTI cURL

// create both cURL resources
if ($second_form) {
	$ch1 = curl_init(); // Mailchimp
	$ch2 = curl_init(); // DAP
	
	// set URL and other appropriate options
	curl_setopt($ch1, CURLOPT_URL, $url1);
	curl_setopt($ch2, CURLOPT_URL, $url2);
	curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch1, CURLOPT_POST, true);
	curl_setopt($ch2, CURLOPT_POST, true);
	curl_setopt($ch1, CURLOPT_POSTFIELDS, $post_fields1);
	curl_setopt($ch2, CURLOPT_POSTFIELDS, $post_fields2);
	
	
	//create the multiple cURL handle
	$mh = curl_multi_init();
	
	//add the two handles
	curl_multi_add_handle($mh,$ch1);
	curl_multi_add_handle($mh,$ch2);
	
	$active = null;
	//execute the handles
	do {
		$mrc = curl_multi_exec($mh, $active);
	} while ($mrc == CURLM_CALL_MULTI_PERFORM);
	
	while ($active && $mrc == CURLM_OK) {
		if (curl_multi_select($mh) != -1) {
			do {
				$mrc = curl_multi_exec($mh, $active);
			} while ($mrc == CURLM_CALL_MULTI_PERFORM);
		}
	}
	
	//close the handles
	curl_multi_remove_handle($mh, $ch1);
	curl_multi_remove_handle($mh, $ch2);
	curl_multi_close($mh);
}

if (!empty($url1)) {
	
// WE ARE ONLY USING 1 FORM...SINGLE cURL
	
$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields1);
	//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	$result = curl_exec ($ch);

	curl_close($ch);
}

// LET'S REDIRECT

// Check If DAP Auto Login and redirect.
//$site_url = get_bloginfo('url');
//if ($auto_login){
  //header('Location:'. $site_url . '/dap/authenticate.php?login=Y&email='.$email.'&redirect=' . $redirect );
	
//} else {
header('Location:'. $redirect );
//}

?>