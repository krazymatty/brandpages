<?php 
/*               LET'S BUILD POST VARIBLES FOR THE CURL
*****************************************************************************/
//set POST variables for the first form
$fields1 = array(
						$lname_field=>urlencode($last_name),
						$fname_field=>urlencode($first_name),
						$email_field=>urlencode($email),
						$city_field=>urlencode($city),
						$state_field=>urlencode($state),
						$birthday_field=>urlencode($birthday),
						$gender_field=>urlencode($gender),
						$username_field=>urlencode($username),
						$password_field=>urlencode($password),
						$phone_field=>urlencode($phone),
						$userid_field=>urlencode($user_id),
			);
			
//url-ify the data for the POST
foreach($fields1 as $key=>$value) { $fields_string1 .= $key.'='.$value.'&'; }
rtrim($fields_string1,'&');

// If we have dynamic form data let's add it to our Post Variables
if ($formFields1) {
	$result = array();
	
	foreach ($formFields1 as $key => $value)
	{
	  $tmp = array_values($value);
	  $result[$tmp[0]] = $tmp[1];
	}
	$form_fields1 = (http_build_query($result));


	$post_fields1 = $form_fields1 . '&' . $fields_string1;
} else {
	
	$post_fields1 = $fields_string1;
	  
}

/*              SETUP SECOND cURL FUNCTION If BOTH FORMS ARE ACTIVE
*******************************************************************************/
if ($second_form) {
//set POST variables
$fields2 = array(
						$lname_field2=>urlencode($last_name),
						$fname_field2=>urlencode($first_name),
						$email_field2=>urlencode($email),
						$birthday_field2=>urlencode($birthday),
						$gender_field2=>urlencode($gender),
						$username_field2=>urlencode($username),
						$password_field2=>urlencode($password),
						$city_field2=>urlencode($city),
						$state_field2=>urlencode($state),
						$phone_field2=>urlencode($phone),
						$userid_field2=>urlencode($user_id),
			);

//url-ify the data for the POST
foreach($fields2 as $key=>$value) { $fields_string2 .= $key.'='.$value.'&'; }
rtrim($fields_string2,'&');

// If we have dynamic form data let's add it to our Post Variables
if ($formFields2) {
	$results = array();
	
	foreach ($formFields2 as $key => $value)
	{
	  $tmp = array_values($value);
	  $results[$tmp[0]] = $tmp[1];
	}
	$form_fields2 = (http_build_query($results));


	$post_fields2 = $form_fields2 . '&' . $fields_string2;
} else {
	
	$post_fields2 = $fields_string2;
	}
}

?>