<?php 
/*   POST META DATA
********************************************************************/
// Let's get the Meta Data from the forms and put them into variables.

$fname_field = get_post_meta($post->ID, VWT_PREFIX.'fname', true);
$lname_field = get_post_meta($post->ID, VWT_PREFIX.'lname', true);
$email_field = get_post_meta($post->ID, VWT_PREFIX.'email', true);
$birthday_field = get_post_meta($post->ID, VWT_PREFIX.'birthday', true);
$gender_field = get_post_meta($post->ID, VWT_PREFIX.'gender', true);
$city_field = get_post_meta($post->ID, VWT_PREFIX.'city', true);
$state_field = get_post_meta($post->ID, VWT_PREFIX.'state', true);
$phone_field = get_post_meta($post->ID, VWT_PREFIX.'phone', true);
$username_field = get_post_meta($post->ID, VWT_PREFIX.'username', true);
$password_field = get_post_meta($post->ID, VWT_PREFIX.'password', true);
$url1 = get_post_meta($post->ID, VWT_PREFIX.'action_url', true);
$userid_field = get_post_meta($post->ID, VWT_PREFIX.'user_id', true);
$redirect = get_post_meta($post->ID, VWT_PREFIX.'redirect', true);
$fb_redirect = get_post_meta($post->ID, VWT_PREFIX.'fb_redirect', true);
$fb_redirect_link = get_post_meta($post->ID, VWT_PREFIX.'fb_redirect_link', true);
$auto_login = get_post_meta($post->ID, VWT_PREFIX.'auto_login', true);

//get the dyamic form meta data array
$formFields1 = get_post_meta($post->ID,'_diy1_formFields',true);

// Check to see if we are redirecting to a fanpage.
if ($fb_redirect) {
	// check to see if we are needing a specific page and if so let's build the redirect link.
	if ($fb_redirect_link) {
	$redirect = $redirect.'&app_data=link,'.$fb_redirect_link;
	}  
}
// Find out if the second form been activated.
$second_form = get_post_meta($post->ID, VWT_PREFIX.'second_form', true);

// If the second form is activated then we'll setup the 2nd form fields array
if ($second_form) {
  $fname_field2 = get_post_meta($post->ID, VWT_PREFIX.'fname2', true);
  $lname_field2 = get_post_meta($post->ID, VWT_PREFIX.'lname2', true);
  $email_field2 = get_post_meta($post->ID, VWT_PREFIX.'email2', true);
  $birthday_field2 = get_post_meta($post->ID, VWT_PREFIX.'birthday2', true);
  $gender_field2 = get_post_meta($post->ID, VWT_PREFIX.'gender2', true);
  $city_field2 = get_post_meta($post->ID, VWT_PREFIX.'city2', true);
  $state_field2 = get_post_meta($post->ID, VWT_PREFIX.'state2', true);
  $phone_field2 = get_post_meta($post->ID, VWT_PREFIX.'phone2', true);
  $username_field2 = get_post_meta($post->ID, VWT_PREFIX.'username2', true);
  $password_field2 = get_post_meta($post->ID, VWT_PREFIX.'password2', true);
  $url2 = get_post_meta($post->ID, VWT_PREFIX.'action_url2', true);
  $userid_field2 = get_post_meta($post->ID, VWT_PREFIX.'user_id2', true);
  //get the dyamic form meta data array
  $formFields2 = get_post_meta($post->ID,'_diy2_formFields',true);
}

// Find out if the second form been activated.
$mailchimp = get_post_meta($post->ID, VWT_PREFIX.'mc_on', true);
if ($mailchimp) {
	  $mc_api = get_post_meta($post->ID, VWT_PREFIX.'mc_api', true);
	  $mc_data_server = get_post_meta($post->ID, VWT_PREFIX.'mc_data_server', true);
	  $mc_list_id = get_post_meta($post->ID, VWT_PREFIX.'mc_list_id', true);
	  $mc_double_opt = get_post_meta($post->ID, VWT_PREFIX.'mc_double_opt', true);
	  $mc_overwrite = get_post_meta($post->ID, VWT_PREFIX.'mc_overwrite', true);
	  $mc_update = get_post_meta($post->ID, VWT_PREFIX.'mc_update', true);
	  $mc_send_welcome = get_post_meta($post->ID, VWT_PREFIX.'mc_send_welcome', true);
}

//Let's Get the Facebook signed request.
?>