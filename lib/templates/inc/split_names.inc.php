<?php 
/*               Split Names
**************************************************/
// Split First and Last Name Check for Suffix.
$name = trim($name);
$name_parts = explode( ' ', $name);
$reverse = array_reverse($name_parts);
$first_name = $name_parts[0];
if ( is_null($name_parts[2]) ) //Meaning only two names were entered...
{
    $last_name = $name_parts[1];
} else {

$suffix = array('jr','sr','ii','iii','iv');
// then I would do the following
$suffix_implode = implode('|', $suffix);
$prefix = array('vere','von','van','de','del','della','di','da','pietro','vanden','du','st.','st','la','ter','mc');
$prefix_implode = implode('|', $prefix);

if (preg_match('/'. $suffix_implode .'/i', $reverse[0])) {
	 $last_name = $reverse[1];
}

if (preg_match('/'. $prefix_implode .'/i', $name_parts[1])) {
  	  $last_name = $name_parts['3'];
	  if (!$name_parts[3]) {
	  $last_name = $name_parts['1'] . ' ' . $name_parts['2'];
	  }
}
if (preg_match('/'. $prefix_implode .'/i', $name_parts[2])) {
	  $last_name = $name_parts['2'] . ' ' . $name_parts['3'];
}
}
?>