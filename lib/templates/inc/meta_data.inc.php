<?php 
/*  GETS THE OPTION'S PAGE AND META POST DATA
************************************************************/

/*******************************************
*    SETTINGS MENU
*******************************************/
$fb_app_id = get_option( VWT_PREFIX.'fb_app_id' );
$fb_app_secret = get_option( VWT_PREFIX.'fb_app_secret' );
$fanpage_width =  '810'; //get_option(VWT_PREFIX.'fanpage_width', true);
$fb_user_id = get_option( VWT_PREFIX.'fb_user_id' );
/*******************************************
*    FAN PAGE META DATA 
*******************************************/
$meta_data = get_post_meta($post->ID);

$style = $meta_data[VWT_PREFIX.'style'][0];
$css = $meta_data[VWT_PREFIX.'css'][0];
$menu_above = $meta_data[VWT_PREFIX.'menu_above'][0];
$menu_below = $meta_data[VWT_PREFIX.'menu_below'][0];
$header_image_url = $meta_data[VWT_PREFIX.'header_image'][0];
$header_height = $meta_data[VWT_PREFIX.'header_height'][0];
$header_shadow = $meta_data[VWT_PREFIX.'header_shadow'][0];
$sidebar_layout = $meta_data[VWT_PREFIX.'sidebar_layout'][0];
$fb_login = $meta_data[VWT_PREFIX.'login_checkbox'][0];
$fb_login_url = $meta_data[VWT_PREFIX.'lo_redirect_url'][0];
//$lo_contest = $meta_data[VWT_PREFIX.'lo_contest'][0];
//$lo_conversions = $meta_data[VWT_PREFIX.'lo_number_of_conversions'][0];
//$lo_remaining_conversions = $meta_data[VWT_PREFIX .'lo_remaining_conversions'][0];
//$lo_redirect_url = $meta_data[VWT_PREFIX.'lo_redirect_url'][0];
//$lo_expired_content = $meta_data[VWT_PREFIX.'lo_expired_content'][0];
$reveal = $meta_data[VWT_PREFIX.'reveal_checkbox'][0];
$redirect_url = $meta_data[VWT_PREFIX.'redirect_url'][0];
$reveal_content = $meta_data[VWT_PREFIX.'reveal_content'][0];
$timer_redirect = $meta_data[VWT_PREFIX.'timer_redirect_url'][0];
$timer_content = $meta_data[VWT_PREFIX.'timer_content'][0];
$conversions = $meta_data[VWT_PREFIX.'number_of_conversions'][0];
$lo_timmer_redirect = $meta_data[VWT_PREFIX.'lo_timer_redirect_url'][0];
$lo_timer_content = $meta_data[VWT_PREFIX.'lo_timer_content'][0];
$app_id = $meta_data[VWT_PREFIX.'fb_app_id'][0];
$app_secret = $meta_data[VWT_PREFIX.'fb_app_secret'][0];
$affiliate_id = $meta_data[VWT_PREFIX.'affiliate_id'][0];
$disable_credit = $meta_data[VWT_PREFIX.'disable_credit'][0];  // Plugin credit
/*******************************************
*    FB REGISTER META DATA
*******************************************/
$fname_field = $meta_data[VWT_PREFIX.'fname'][0];
$lname_field = $meta_data[VWT_PREFIX.'lname'][0];
$email_field = $meta_data[VWT_PREFIX.'email'][0];
$birthday_field = $meta_data[VWT_PREFIX.'birthday'][0];
$gender_field = $meta_data[VWT_PREFIX.'gender'][0];
$city_field = $meta_data[VWT_PREFIX.'city'][0];
$state_field = $meta_data[VWT_PREFIX.'state'][0];
$phone_field = $meta_data[VWT_PREFIX.'phone'][0];
$username_field = $meta_data[VWT_PREFIX.'username'][0];
$password_field = $meta_data[VWT_PREFIX.'password'][0];
$url1 = $meta_data[VWT_PREFIX.'action_url'][0];  // the action url of the form
$userid_field = $meta_data[VWT_PREFIX.'user_id'][0];  // the captured users facebook id
$fb_redirect = $meta_data[VWT_PREFIX.'fb_redirect'][0]; // checkbox to see if the redirect is a fanpage
$redirect = $meta_data[VWT_PREFIX.'redirect'][0];  // the fanpage url  
$fb_redirect_link = $meta_data[VWT_PREFIX.'fb_redirect_link'][0];  //  the specific page onthe website to display in the iframe
$auto_login = $meta_data[VWT_PREFIX.'auto_login'][0];  // dap auto-loing
/*******************************************
*    FB LINK META DATA
*******************************************/
$fanpage_title = $meta_data[VWT_PREFIX.'fanpage_title'][0];
$fanpage_desc = $meta_data[VWT_PREFIX.'fanpage_desc'][0];
$fanpage_images = $meta_data[VWT_PREFIX.'fanpage_images'][0];
$fb_images = preg_split('/,/', $fanpage_images);
$sendto_fanpage_url = $meta_data[VWT_PREFIX.'sendto_fanpage_url'][0];
$post_fanpage_url = $meta_data[VWT_PREFIX.'post_fanpage_url'][0];
$link_url = $meta_data[VWT_PREFIX.'link_url'][0];
$og_type = $meta_data[VWT_PREFIX.'og_type'][0];
$video_url = $meta_data[VWT_PREFIX.'video_url'][0];
$video_width = $meta_data[VWT_PREFIX.'video_width'][0];
$video_height = $meta_data[VWT_PREFIX.'video_height'][0];
$permalink = get_permalink();


/*  DOES THE LAYOUT HAVE A SIDEBAR
************************************************************/
if (empty($sidebar_layout)) {
  $sidebar_layout = 'No'; }

/*  DETERMINES HEADER HEIGHT
************************************************************/
if ($header_height != '') {
  $header_height = "
  #diy-header { height: " . $header_height . 'px; background-repeat: no-repeat; background-position: center; 
}';
  }

/*  HEADER IMAGE
************************************************************/
if ($header_image_url) {
  $header_image = '#diy-header {background:url(' . $header_image_url . '); }';
} 

/*  SET FACEBOOK APP ID AND SECRET
************************************************************/
if (empty($app_id)) {
  $app_id = $fb_app_id; }
if (empty($app_secret)) {
  $app_secret = $fb_app_secret; }

/* SET THE REDIRECT AND REVEAL PAGE
************************************************************/
$redirect = $meta_data[VWT_PREFIX.'redirect_url'][0];
$content = $meta_data[VWT_PREFIX.'reveal_content'][0];



//get the dyamic form meta data array
$formFields1 = get_post_meta($post->ID,'formFields',true);

// Find out if the second form been activated.
$second_form = $meta_data[VWT_PREFIX.'second_form'][0];

// Check to see if we are redirecting to a fanpage.
if ($fb_redirect) {
	// check to see if we are needing a specific page and if so let's build the redirect link.
	if ($fb_redirect_link) {
	$redirect = $redirect.'&app_data=link,'.$fb_redirect_link;}  
}

// If the second form is activated then we'll setup the 2nd form fields array
if ($second_form) {
  $fname_field2 = $meta_data[VWT_PREFIX.'fname2'][0];
  $lname_field2 = $meta_data[VWT_PREFIX.'lname2'][0];
  $email_field2 = $meta_data[VWT_PREFIX.'email2'][0];
  $birthday_field2 = $meta_data[VWT_PREFIX.'birthday2'][0];
  $gender_field2 = $meta_data[VWT_PREFIX.'gender2'][0];
  $city_field2 = $meta_data[VWT_PREFIX.'city2'][0];
  $state_field2 = $meta_data[VWT_PREFIX.'state2'][0];
  $phone_field2 = $meta_data[VWT_PREFIX.'phone2'][0];
  $username_field2 = $meta_data[VWT_PREFIX.'username2'][0];
  $password_field2 = $meta_data[VWT_PREFIX.'password2'][0];
  $url2 = $meta_data[VWT_PREFIX.'action_url2'][0];
  $userid_field2 = $meta_data[VWT_PREFIX.'user_id2'][0];
  //get the dyamic form meta data array
  $formFields2 = $meta_data['formFields2'][0];
}
// Find out if the second form been activated.
/*$mailchimp = $meta_data[VWT_PREFIX.'mc_on'][0];
if ($mailchimp) {
	  $mc_api = $meta_data[VWT_PREFIX.'mc_api'][0];
	  $mc_data_server = $meta_data[VWT_PREFIX.'mc_data_server'][0];
	  $mc_list_id = $meta_data[VWT_PREFIX.'mc_list_id'][0];
	  $mc_double_opt = $meta_data[VWT_PREFIX.'mc_double_opt'][0];
	  $mc_overwrite = $meta_data[VWT_PREFIX.'mc_overwrite'][0];
	  $mc_update = $meta_data[VWT_PREFIX.'mc_update'][0];
	  $mc_send_welcome = $meta_data[VWT_PREFIX.'mc_send_welcome'][0];
}

//Let's Get the Facebook signed request.
*/

?>