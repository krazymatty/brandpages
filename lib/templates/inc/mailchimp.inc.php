<?php 
// Find out if we are sending to mailchimp.
$mailchimp = get_post_meta($post->ID, VWT_PREFIX.'mc_on', true);
if ($mailchimp) {
  $mc_api = get_post_meta($post->ID, VWT_PREFIX.'mc_api', true);
  $mc_data_server = get_post_meta($post->ID, VWT_PREFIX.'mc_data_server', true);
  $mc_list_id = get_post_meta($post->ID, VWT_PREFIX.'mc_list_id', true);
  $mc_double_opt = get_post_meta($post->ID, VWT_PREFIX.'mc_double_opt', true);
  $mc_overwrite = get_post_meta($post->ID, VWT_PREFIX.'mc_overwrite', true);
  $mc_update = get_post_meta($post->ID, VWT_PREFIX.'mc_update', true);
  $mc_send_welcome = get_post_meta($post->ID, VWT_PREFIX.'mc_send_welcome', true);

  $birthdate = date('m/d', strtotime($birthday)); 
  $phonenumber = str_replace("-", "", $phone);
  
  $merges = array('FNAME'=>$first_name, 'LNAME'=>$last_name,
  'address1'=>array('addr1'=>'', 'city'=>$city, 'state'=>$state, 'zip'=>''),
  'phone' => $phonenumber,
  'birthday'=>$birthdate,
  );

  $email_type = 'html';
  $data = array(
  'email_address'=>$email,
  'apikey'=>$mc_api,
  'merge_vars' => $merges,
  'id' => $mc_list_id,
  'double_optin' => $mc_double_opt,
  'update_existing' => $mc_overwrite,
  'replace_interests' => $mc_update,
  'send_welcome' => $mc_send_welcome,
  'email_type' => $email_type
  );
  $payload = json_encode($data);
   
  //replace us2 with your actual datacenter
  $submit_url = 'http://'.$mc_data_server.'.api.mailchimp.com/1.3/?method=listSubscribe';
   
  $mch = curl_init();
  curl_setopt($mch, CURLOPT_URL, $submit_url);
  curl_setopt($mch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($mch, CURLOPT_POST, true);
  curl_setopt($mch, CURLOPT_POSTFIELDS, urlencode($payload));
} 
?> 