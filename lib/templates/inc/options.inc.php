<?php 
/**----------:[ GETS THE OPTIONS PAGE AND META POST SETTINGS ]:-------------------**/

$fb_app_id = get_option( VWT_PREFIX.'fb_app_id' );
$fb_app_secret = get_option( VWT_PREFIX.'fb_app_secret' );
$app_id = get_post_meta($post->ID, VWT_PREFIX.'app_id', true);
$app_secret = get_post_meta($post->ID, VWT_PREFIX.'app_secret', true);

if (empty($app_id)) {
  $app_id = $fb_app_id; }
if (empty($app_secret)) {
  $app_secret = $fb_app_secret; }

define('FACEBOOK_APP_ID', $app_id); // Place your App Id here
define('FACEBOOK_SECRET', $app_secret); // Place your App Secret Here

?>