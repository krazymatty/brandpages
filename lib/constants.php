<?php
/**
 * Constants used by this plugin
 * 
 * @package DIYBRANDPAGES
 * 
 * @author kynatro
 * @version 1.0.0
 * @since 1.0.0
 */

$VWT_license='Standard';//Standard or Developer
$VWT_version='1.0.0';//Standard or Developer
$VWT_plugin_name='DIY Brand Pages';
$VWT_prefix='_diy_';
$VWT_site_url='https://diybrandpages.com';
$VWT_support_url='https://diybrandpages.com/support';

// The wordpress prefix
if( !defined( 'VWT_WP_PREFIX' ) )define( 'VWT_WP_PREFIX', $wpdb->prefix );
// The License
if( !defined( 'VWT_LICENSE' ) )define( 'VWT_LICENSE',$VWT_license);
// The plugin Name
if( !defined( 'VWT_PLUGIN_NAME' ) )define( 'VWT_PLUGIN_NAME',$VWT_plugin_name);
// The plugin prefix
if( !defined( 'VWT_PREFIX' ) )define( 'VWT_PREFIX',$VWT_prefix);
// The website url
if( !defined( 'VWT_SITE_URL' ) )define( 'VWT_SITE_URL',$VWT_site_url);
// The Support url
if( !defined( 'VWT_SUPPORT_URL' ) )define( 'VWT_SUPPORT_URL',$VWT_support_url);
// The current version of this plugin
if( !defined( 'VWT_VERSION' ) ) define( 'VWT_VERSION', $VWT_version );
// The directory the plugin resides in
if( !defined( 'VWT_DIRURL' ) ) define( 'VWT_DIRURL', plugin_dir_url(__FILE__) );
// The URL path of this plugin
if( !defined( 'VWT_DIRPATH' ) ) define( 'VWT_DIRPATH', plugin_dir_path(__FILE__) );
// The name of the plugin
if( !defined( 'VWT_URLBASE' ) ) define( 'VWT_URLBASE', plugin_basename( __FILE__ ) );
if( !defined( 'IS_AJAX_REQUEST' ) ) define( 'IS_AJAX_REQUEST', ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) );

?>