<?php
/****************************************************************
*     SETTINGS PAGE
*****************************************************************/

	// add the admin options page
$diy_options = array (
 
		array( "name" => VWT_PLUGIN_NAME." Options",
			"type" => "title"),
		 
		array( "name" => "Activation",
			"type" => "section"),
		array( "type" => "open"),
		 
		array( "name" => "License Key",
			"desc" => "Enter your License Key",
			"id" => VWT_PREFIX."activation_key",
			"type" => "text",
			"std" => ""),
			
		array( "name" => "Email",
			"desc" => "Enter the email you used when you registerd your ".VWT_PLUGIN_NAME." Plugin ",
			"id" => VWT_PREFIX."registered_email",
			"type" => "text",
			"std" => ""),
			
		array( "type" => "close"),
		array( "name" => "Facebook Application Setup",
			"desc" => "Enter Your Facebook App Id & Secret",
			"type" => "section"),
		array( "type" => "open"),
		array( "name" => "Facebook User ID",
			"desc" => "Enter Your facebook User ID.  Find It Here: https://graph.facebook.com/YourUserName",
			"id" => VWT_PREFIX."fb_user_id",
			"type" => "text",
			"std" => ""),
		array( "name" => "Facebook App ID",
			"desc" => "Enter Your facebook Application ID",
			"id" => VWT_PREFIX."fb_app_id",
			"type" => "text",
			"std" => ""),
			
		array( "name" => "Facebook App Secret",
			"desc" => "Enter Your facebook Application Secret",
			"id" => VWT_PREFIX."fb_app_secret",
			"type" => "text",
			"std" => ""),
		array( "type" => "close"),
		array( "name" => "Plugin Setup",
			"desc" => "Change Plugin Defaults",
			"type" => "section"),
		array( "type" => "open"),
		array( "name" => "Fanpage Directory",
			"desc" => "Change your Fanpages directory.<br/>Current: ".VWT_DIRURL."/<span style='color:black;'>".FANPAGE_DIR."/</span><br/><span style='color:red;'><em>You MUST Update <a href='/wp-admin/options-permalink.php'>Permalinks!</a></span></em>",
			"id" => VWT_PREFIX."fanpage_dir",
			"type" => "text",
			"std" => ""),
		array( "name" => "FB Register Directory",
			"desc" => "Change your FB-Register directory.<br/>Current: ".VWT_DIRURL."/<span style='color:black;'>".FANPAGE_DIR_REGISTER."/</span><br/><span style='color:red;'><em>You MUST Update <a href='/wp-admin/options-permalink.php'>Permalinks!</a></span></em>",
			"id" => VWT_PREFIX."fanpage_dir_register",
			"type" => "text",
			"std" => ""),
		array( "name" => "Redirect Link Directory",
			"desc" => "Change you Redirect Directory.<br/>Current: ".VWT_DIRURL."/<span style='color:black;'>".FANPAGE_DIR_REDIRECT."/</span><br/><span style='color:red;'><em>You MUST Update <a href='/wp-admin/options-permalink.php'>Permalinks!</a></span></em>",
			"id" => VWT_PREFIX."fanpage_dir_redirect",
			"type" => "text",
			"std" => ""),
		array( "name" => "Fanpage Url",
			"desc" => "Enter Your facebook fanpage url.",
			"id" => VWT_PREFIX."fanpage_url",
			"type" => "text",
			"std" => ""),
		array( "name" => "Fanpage Width",
			"desc" => "Enter Facebook Page Width. 520 or 810",
			"id" => VWT_PREFIX."fanpage_width",
			"type" => "text",
			"std" => "810"),
		array( "type" => "close"),
		array( "name" => "Make Money with ".VWT_PLUGIN_NAME."",
			"type" => "section"),
		array( "type" => "open"),
			
		array( "name" => "Affiliate ID",
			"desc" => "Enter Your Affiliate ID from the members area.",
			"id" => VWT_PREFIX."affiliate_id",
			"type" => "text",
			"std" => ""),
			
		/*array( "name" => "Google Analytics Code",
			"desc" => "You can paste your Google Analytics or other tracking code in this box. This will be automatically added to the footer.",
			"id" => VWT_PREFIX."ga_code",
			"type" => "textarea",
			"std" => ""),	
			
		array( "name" => "Custom Favicon",
			"desc" => "A favicon is a 16x16 pixel icon that represents your site; paste the URL to a .ico image that you want to use as the image",
			"id" => VWT_PREFIX."favicon",
			"type" => "text",
			"std" => get_bloginfo('url') ."/favicon.ico"),	
		*/	
		 
		array( "type" => "close")
 
);

function diy_add_options_page() {
	global $diy_options;
	global $diy_registered_email, $diy_activation_key, $diy_check_date, $curdate;
	global $wpdb;
	if ( $_GET['page'] == 'diy-fanpage') {
	 
		if ( 'save' == $_REQUEST['action'] ) {
	 
			foreach ($diy_options as $value) {
			update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }
	 
	foreach ($diy_options as $value) {
		if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } 
	}
		header('Location: admin.php?page=diy-fanpage&saved=true');
		die;
	}
	elseif( 'reset' == $_REQUEST['action'] ) {
	 
		foreach ($diy_options as $value) {
			delete_option( $value['id'] ); }
	 
		header('Location: admin.php?page=diy-fanpage&reset=true');
		die; 
		}
	}
}

function init_diy_setup_page() {
	wp_enqueue_style("options", VWT_DIRURL ."classes/meta-box/css/options.css", false, "1.0", "all");
	wp_enqueue_script("rm_script", VWT_DIRURL ."classes/meta-box/js/rm_script.js", false, "1.0");
}

function diy_settings_page(){
	global $diy_options;
	$i = 0;
	$ni = 0;
	 
	if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.VWT_PLUGIN_NAME.' settings saved.</strong></p></div>';
	if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.VWT_PLUGIN_NAME.' settings reset.</strong></p></div>';
	 
	?>
	<div class="wrap rm_wrap">
		<h2><?php echo VWT_PLUGIN_NAME; ?> Settings</h2>
		<div class="rm_opts">
		<form method="post">
		<?php foreach ($diy_options as $value) {
			switch ( $value['type'] ) {
	
			case "section":
		
			$i++;    ?>
			
			<div class="rm_section">
				<div class="rm_title">
					<h3><img src="<?php echo DIY_URL;?>images/trans.gif" class="inactive" alt="""><?php echo $value['name']; ?><span style="text-transform:none; margin-left:10px;"><?php echo $value['desc']; ?></span></h3>
					<span class="submit">
					<input name="save<?php echo $i; ?>" type="submit" value="Save changes" />
					</span>
					<div class="clearfix"></div>
				</div>
			<div class="rm_options">
			<?php break;
			 
			case "open":
			break;
	
			case "title":
			?>
			<p>Fill out the Activation form below to Activate the <?php echo VWT_PLUGIN_NAME;?> plugin. <span style="font-weight:bold; color:#900;">Status:
				<?php if (get_option(VWT_PREFIX . 'status')) { echo get_option(VWT_PREFIX . 'status');} else { echo 'Not Activated';} ?>
				</span></p>
			<p>Version <?php echo VWT_VERSION; ?></p>
			<?php break;
			 
			case 'text':
			?>
			<div class="rm_input rm_text">
				<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
				<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id'])  ); } else { echo $value['std']; } ?>" />
				<small><?php echo $value['desc']; ?></small>
				<div class="clearfix"></div>
			</div>
			<?php
			break;
			 
			case 'textarea':
			?>
			<div class="rm_input rm_textarea">
				<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
				<textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id']) ); } else { echo $value['std']; } ?>
				</textarea>
				<small><?php echo $value['desc']; ?></small>
				<div class="clearfix"></div>
			</div>
			<?php
			break;
			 
			case 'select':
			?>
			<div class="rm_input rm_select">
				<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
				<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
					<?php foreach ($value['options'] as $option) { ?>
					<option <?php if (get_option( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option>
					<?php } ?>
				</select>
				<small><?php echo $value['desc']; ?></small>
				<div class="clearfix"></div>
			</div>
			<?php
			break;
			 
			case "checkbox":
			?>
			<div class="rm_input rm_checkbox">
				<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
				<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
				<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
				<small><?php echo $value['desc']; ?></small>
				<div class="clearfix"></div>
			</div>
			<?php break; 
			
			case "close":     ?>
			
			</div>
			</div>
			<br />
			<?php break; 
			} 
		} ?>
			
			<input type="hidden" name="action" value="save" />
			</form>
			<form method="post">
				<p class="submit">
					<input name="reset" type="submit" value="Total Reset" />
					<input type="hidden" name="action" value="reset" />
				</p>
			</form>
		</div>
	</div>
<?php
}
add_action( 'admin_init', 'init_diy_setup_page' );
add_action('admin_menu', 'diy_add_options_page');
?>