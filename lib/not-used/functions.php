<?php 
//include('lib/settings.inc.php');
function vwt_settings_page() {
	echo "hello";
	}


/**************************************************************
*     REGISTER SHORTCODES
***************************************************************/
function facebook_registration_form($atts){  
	extract(shortcode_atts( array('url' => 'url', 'width' => 'width'), $atts)); 
	if (empty($width)) { $width = '520';} 
	$return = '<!--Facebook Registration Plugin--><fb:registration fields="[{\'name\':\'name\'},{\'name\':\'first_name\'},{\'name\':\'last_name\'},{\'name\':\'email\'},{\'name\':\'phone\'},{\'name\':\'password\'},{\'name\':\'location\'}]"';  
	$return .= 'redirect-uri="'.$url.'" width="'.$width.'" ></fb:registration>';  
  
	return $return;   
}  
//add_shortcode('facebookform', 'facebook_registration_form');  

function facebook_comments_form($atts){  
	extract(shortcode_atts( array('posts' => 'posts', 'width' => 'width'), $atts));
	$post_url = get_permalink($post->ID); 
	if (empty($width)) { $width = '520';} 
	if (empty($posts)) { $posts = '2';} 
	$return = '<!--Facebook Comments-->
<div class="fb-comments" data-href="'.$post_url.'" data-num-posts="'.$posts.'" data-width="'.$width.'"></div>';  
  
	return $return;   
}  
//add_shortcode('facebookcomments', 'facebook_comments_form');  

function tinyplugin_add_button($buttons)
{
    array_push($buttons, "separator", "facebookform",$buttons, "separator", "facebookcomments");
    return $buttons;
}
 
function tinyplugin_register($plugin_array)
{
    $url = get_bloginfo('url');
    $url .= "/wp-content/plugins/diy-brand-pages/lib/inc/facebookform.js";
 
    $plugin_array["facebookform"] = $url;
    $plugin_array["facebookcomments"] = $url;
    return $plugin_array;
}
//add_filter('mce_external_plugins', "tinyplugin_register");
//add_filter('mce_buttons', 'tinyplugin_add_button', 0);

/**************************************************************
*     REGISTER NEW SIDEBAR FOR FANPAGES
***************************************************************/
function self_deprecating_sidebar_registration(){
register_sidebar(array('name'=>'Fanpage', 'id'=>'diy-sidebar', 'before_title'=>'<h2>', 'after_title'=>'</h2>'));
}

//add_action( 'wp_loaded', 'self_deprecating_sidebar_registration' );

/**************************************************************
*     IF DE-ACTIVATED
***************************************************************/
function diy_deactivate(){
	delete_option('_diy_check_date');
	delete_option(VWT_PREFIX . 'permalink_notice');
	delete_option(VWT_PREFIX . 'permalink_update');
	delete_option(VWT_PREFIX . 'status');
	delete_option(VWT_PREFIX . 'registered_email');
	delete_option(VWT_PREFIX . 'activation_key');
	delete_option(VWT_PREFIX . 'fanpage_dir');
	delete_option(VWT_PREFIX . 'fanpage_dir_register');
	delete_option(VWT_PREFIX . 'fanpage_dir_redirect'); 

}
/**************************************************************
*    REFRESH PERMALINKS
***************************************************************/
function refresh_permalinks() {
	global $wp_rewrite;
	$link_status = get_option(VWT_PREFIX . 'permalink_update');
	if (!$link_status) { flush_rewrite_rules();
	update_option(VWT_PREFIX . 'permalink_update', true);} 
}
/**************************************************************
*    ADD MENU TO TEMPLATE
***************************************************************/
//register_nav_menu('fanpage_menu','Fanpage Menu');

/**************************************************************
*    ADD RSS WIDGETS
***************************************************************/
function smtfeed_setup_function() {
	global $rss_title;

    add_meta_box( 'smtfeed_widget', ''.stripslashes($rss_title).'', 'smtfeed_widget_function', 'dashboard', 'normal', 'high' );
}
function smtfeed_widget_function() {
    require_once ('rss_feed.inc.php'); // new file where RSS Feed will be called from
    echo '<style type="text/css">
    #smt-message { background: #E5F5FF; border: 1px solid #80CCFF; padding:5px; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; margin-bottom:10px; }
    #smt-important-message { background: #FFEBE8; border: 1px solid #CC0000; padding:5px; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; margin-bottom:10px; }
    #smtfeed_widget a:hover, #smtfeed_widget a:active, #smtfeed_widget a:focus {color: #003366;}
    #smtfeed_widget .handlediv {display:none;}
    </style>';
}
/**************************************************************
*    ADD CUSTOM COLUMNS TO FANPAGE
***************************************************************/
function add_diy_fanpage_columns( $cols ) {
  $cols = array(
  'cb'       => '<input type="checkbox" />',
  'title'       => __( 'Title'),
  'fanpageUrl'      => __( 'Canvas Page Tab Url',      'trans' ),
  'likeGate'       => __( 'Like Gate Redirects to:',  'trans'),
  'date'      => __( 'Date',      'trans' ),
  );
  return $cols;
}
//add_filter( "manage_diy-fanpage_posts_columns", "add_diy_fanpage_columns" );

function custom_diy_fanpage_columns( $column, $post_id ) {
	global $post;
  switch ( $column ) {
    case "fanpageUrl":
      $fanpageUrl = get_permalink( $post_id );
      echo "<a href='$fanpageUrl'>$fanpageUrl</a>";
      break;
    case "likeGate":
      $redirectUrl = get_post_meta( $post_id,VWT_PREFIX . 'redirect_url' );
      echo "<a href='$redirectUrl[0]'>$redirectUrl[0]</a>";
      break;
  }
}

//add_action( "manage_diy-fanpage_posts_custom_column", "custom_diy_fanpage_columns", 10, 2 );
/**************************************************************
*    ADD CUSTOM COLUMNS TO REDIRECT LINK
***************************************************************/
function add_diy_link_columns( $cols ) {
  $cols = array(
  'cb'       => '<input type="checkbox" />',
  'title'       => __( 'Title'),
  'linkTitle'       => __( 'Link Title'),
  'linkDescription'       => __( 'Link Description'),
  'url'      => __( 'Redirect Url',      'trans' ),
  );
  return $cols;
}
//add_filter( "manage_diy-link_posts_columns", "add_diy_link_columns" );

function custom_diy_link_columns( $column, $post_id ) {
	global $post;
  switch ( $column ) {
    case "linkTitle":
      $facebook = get_post_meta( $post_id,VWT_PREFIX . 'fanpage_title' );
      echo $facebook[0];
      break;
    case "linkDescription":
      $facebook = get_post_meta( $post_id,VWT_PREFIX . 'fanpage_desc' );
      echo $facebook[0];
      break;
    case "url":
      $url = get_permalink( $post_id );
      echo "<a href='$url'>$url</a>";
      break;
  }
}

//add_action( "manage_diy-link_posts_custom_column", "custom_diy_link_columns", 10, 2 );
/**************************************************************
*    ADD CUSTOM COLUMNS TO REGISTRATION FORM
***************************************************************/
function change_diy_register_columns( $cols ) {
  $cols = array(
  'cb'       => '<input type="checkbox" />',
  'title'       => __( 'Title'),
  'location'       => __( 'Location Description'),
  'formurl'      => __( 'Form Action Url',      'trans' ),
  );
  return $cols;
}
//add_filter( "manage_diy-register_posts_columns", "change_diy_register_columns" );

function custom_diy_register_columns( $column, $post_id ) {
	global $post;
  switch ( $column ) {
    case "location":
      $facebook = get_post_meta( $post_id,VWT_PREFIX . 'location_desc' );
      echo $facebook[0];
      break;
    case "formurl":
      $formurl = get_permalink( $post_id );
      echo "<a href='$formurl'>$formurl</a>";
      break;
  }
}

//add_action( "manage_diy-register_posts_custom_column", "custom_diy_register_columns", 10, 2 );
/**************************************************************
*    REMOVE EDITOR ON DIY-REGISTER POST TYPE
***************************************************************/
function remove_editor_from_fb_registration() {
    remove_post_type_support( 'diy-register', 'editor' ); }
//add_action('admin_init', 'remove_editor_from_fb_registration');

function remove_editor_from_fb_links() {
    remove_post_type_support( 'diy-link', 'editor' ); }
//add_action('admin_init', 'remove_editor_from_fb_links');
/**************************************************************
*     ADMIN NOTICES
***************************************************************/
function diy_admin_notice_needs_activated(){
	echo '<div class="error fade" style="width:660px"><p>'.VWT_PLUGIN_NAME.' - <a href=admin.php?page='.FANPAGE_DIR.'>Click here to enter your activation key...</a></p></div>';
}
function diy_admin_notice_not_valid(){
	echo '<div class="error" style="width:660px"><p>'.VWT_PLUGIN_NAME.' - The activation <a href=admin.php?page='.FANPAGE_DIR.'>key is not valid.</a></p></div>';
}
function diy_version_update_notice(){
	echo '<div class="error" style="width:660px; background-color: #E5F5FF; border-color: #80CCFF;"><p>There is an Update to the <a href="https://diybrandpages.com/members/download/" target="_blank" style="color:#3366FF">'.VWT_PLUGIN_NAME.' Plugin.</a></p></div>';
}
/**************************************************************
*     TEMPLATES
***************************************************************/
function diy_fanpage_default_template() {
    if(get_post_type() == 'fanpage') : global $wp_query, $post, $posts;
	  include(DIY_PATH . 'lib/templates/single-diy-fanpage.php');
    exit; endif;
}
//add_action('template_redirect', 'diy_fanpage_default_template');

function diy_register_default_template() {
    if(get_post_type() == 'diy-register') : global $wp_query, $post, $posts;
	  include(DIY_PATH . 'lib/templates/single-diy-register.php');
    exit; endif;
}
//add_action('template_redirect', 'diy_register_default_template');

function diy_link_default_template() {
    if(get_post_type() == 'diy-link') : global $wp_query, $post, $posts;
	  include(DIY_PATH . 'lib/templates/single-diy-link.php');
    exit; endif;
}
//add_action('template_redirect', 'diy_link_default_template');
/**************************************************************
*    CREDIT
***************************************************************/
function diy_fanpages_credit(){
  ?>
    <div id="fb-credit" style="clear:both; text-align:right; margin-right:10px;">Powerd By: <a href="<?php $affiliate_id = get_option(VWT_PREFIX . 'affiliate_id'); if ($affiliate_id) { echo 'https://socialmarketingandtraining.com/dap/a/?a='.$affiliate_id.'=https://socialmarketingandtraining.com/custom-fanpages/';} else {echo 'https://socialmarketingandtraining.com/custom-fanpages/';}; ?>" style="color: #900; text-decoration:none;">DIY Brand Pages</a> </div>
  <?php
}

/*****************************************************************
*      POST TYPES
*****************************************************************/
function diy_fanpage_post_type(){
global $wpdb, $fanpage_dir, $fanpage_dir_register, $fanpage_dir_redirect;
	register_post_type( 'diy-fanpage',
	  array(
		'labels' => array(
		  'name' => __( 'Fan Pages' ),
		  'add_new_item' => __( 'Add New Fan Page' ),
		  'edit_item' => __( 'Edit Fan Page' ),
		  'new_item' => __( 'Fan Page' ),
		  'singular_name' => __( 'Fan Page' )
		  ),
		  'public' => true,
		  'exclude_from_search' => true,
		  'publicly_queryable' => true,
		  'show_ui' => true, 
		  'show_in_menu' => 'diy-fanpage', 
		  'query_var' => true,
		  'rewrite' => array( 'slug' => FANPAGE_DIR ),
		  'capability_type' => 'page',
		  'hierarchical' => true,
		  'has_archive' => true,
		  'menu_position' => 24,
		  'menu_icon' => VWT_DIRNAME .'images/icon.png',
		  'supports' => array('title','editor')
		  )
	);
}
/*****************************************************************
*          CREATE FACEBOOK REGISTRATION POST TYPE 
****************************************************************/
function diy_fbregister_post_type(){
	register_post_type( 'diy-register',
	  array(
		'labels' => array(
		  'name' => __( 'FB Registration Form' ),
		  'add_new_item' => __( 'Add New Form' ),
		  'edit_item' => __( 'Edit Form' ),
		  'new_item' => __( 'Form' ),
		  'singular_name' => __( 'Form' )
		  ),
		  'public' => true,
		  'exclude_from_search' => true,
		  'publicly_queryable' => true,
		  'show_ui' => true, 
  		  'show_in_menu' => false, 
		  'query_var' => true,
		  'rewrite' => array( 'slug' => FANPAGE_DIR_REGISTER),
		  'capability_type' => 'page',
		  'hierarchical' => true,
		  'has_archive' => true,
		  'menu_position' => 25,
		  'menu_icon' => VWT_DIRNAME .'images/icon.png',
		  'supports' => array('title','editor')
		  )
	);
}
/*****************************************************************
*          CREATE FACEBOOK REGISTRATION POST TYPE 
****************************************************************/
function diy_fblink_post_type(){
	register_post_type( 'diy-link',
	  array(
		'labels' => array(
		  'name' => __( 'Redirect To Fanpage' ),
		  'add_new_item' => __( 'Add New Redirect' ),
		  'edit_item' => __( 'Edit Redirect' ),
		  'new_item' => __( 'Redirect Link' ),
		  'singular_name' => __( 'Redirect Link' )
		  ),
		  'public' => true,
		  'exclude_from_search' => true,
		  'publicly_queryable' => true,
		  'show_ui' => true, 
  		  'show_in_menu' => false, 
		  'query_var' => true,
		  'rewrite' => array( 'slug' => FANPAGE_DIR_REDIRECT),
		  'capability_type' => 'page',
		  'hierarchical' => true,
		  'has_archive' => true,
		  'menu_position' => 25,
		  'menu_icon' => VWT_DIRNAME .'images/icon.png',
		  'supports' => array('title','editor')
		  )
	);
}

?>