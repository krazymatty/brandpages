<?php 
$meta_box = new cmb_Meta_Box( array(
		'id' => 'fb_app_setup',
		'title' => 'Facebook Application Setup',
		'render_meta_box_content',
		'pages' => array('diy-register'), // post type
		'context' => 'normal',
		'priority' => 'high',
		'show_names' => true, // Show field names on the left
		'show_ui' => true,
		'fields' => array(
		// FACEBOOK APP SETUP
			array(
				'name' => 'Facebook Application Setup',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Facebook Application Setup',
				'desc' => 'This is where you associate this page to a facebook application.  This will overide the facebook information on the plugin setup page.',
				'type' => 'sub_title',
				'id' => VWT_PREFIX . 'subtitle_fbapp_setup'
			),
			array(
				'name' => 'Facebook Application ID',
				'desc' => '<- This will OVERRIDE the default Application ID on the Setup Page!',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'app_id'
			),
			array(
				'name' => 'Facebook Application Secret',
				'desc' => '<- This will OVERRIDE the default Application Secret on the Setup Page!',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'app_secret'
			),
			array(
				'type' => 'close-left'
			),
			// MAILCHIMP INTEGRATION
			array(
				'name' => 'Mailchimp Integration',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Turn On Mailchimp Integration',
				'desc' => 'Check to "Turn On" Mailchimp!',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_on'
			),
			array(
				'name' => 'Mailchimp API Key',
				'desc' => 'Your mailchimp API key.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'mc_api'
			),
			array(
				'name' => 'Mailchimp Data Server',
				'desc' => 'Your data server looks like this: us4 or us2',
				'type' => 'text_small',
				'id' => VWT_PREFIX . 'mc_data_server'
			),
			array(
				'name' => 'List ID',
				'desc' => 'Your list id.',
				'type' => 'text_small',
				'id' => VWT_PREFIX . 'mc_list_id'
			),
			array(
				'name' => 'Double Opt-in',
				'desc' => 'Check to send your prospet a confirmation email.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_double_opt'
			),
			array(
				'name' => 'Overwrite Existing Contact',
				'desc' => 'This will overwrite a contact that already exist.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_overwrite'
			),
			array(
				'name' => 'Update Contact',
				'desc' => 'This will update the contact.  For example subscriber date.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_update'
			),
			array(
				'name' => 'Send Welcome Email',
				'desc' => 'This will send the Welcome Email that is set for the campaign.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_send_welcome'
			),
			array(
					'type' => 'close-left'
			),
			/*array(
				'name' => 'Digital Access Pass (DAP) SETUP',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'DAP Auto Login',
				'desc' => 'Turns on the DAP Auto Login',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'auto_login'
			),
			array(
					'type' => 'close-left'
			),*/
			// REDIRECT LANDING PAGE
			array(
				'name' => 'Redirect to Fan Page or Thank You/Landing Page',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Form Submission Redirect',
				'desc' => 'This is the url the user will be redirected to after they submit the form.',
				'type' => 'sub_title',
				'id' => VWT_PREFIX . 'subtitle_form_redirect_setup'
			),
			array(
				'name' => 'Are You Redirecting To A Fanpage?',
				'desc' => 'YES!  This is a Fanpage redirect.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'fb_redirect'
			),
			array(
				'name' => 'Fan Page Or Thank You Page URL',
				'desc' => 'Enter URL you would like to redirect to.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'redirect'
			),
			array(
				'name' => '<span style="color:red">Fan Page Only- Specific Page</span>',
				'desc' => 'Enter the URL to the page you would like to display.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'fb_redirect_link'
			),
			array(
					'type' => 'close-left'
			),
/**************************************************************
*         CUSTOM METABOXS AND FIELDS FOR FB REGISTRATION
***************************************************************/
			array(
				'name' => 'Custom Form Submission Setup',
				'desc' => 'This script will allow you to use facebooks registration plugin.  When the user submits the form it will be sent to the script you setup and the information sent to your autoresponders or membership databases.',
				'type' => 'divider',
				'id' => VWT_PREFIX . 'fb_title_register_plugin'
			),
			array(
				'name' => 'Form Script Setup',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Autoresponder/Newsletter Setup',
				'desc' => 'This is where you merge the facebook registration setup with your autoresponder or newsletter form submission code.',
				'type' => 'sub_title',
				'id' => VWT_PREFIX . 'subtitle_autoresponder_setup'
			),
			array(
				'name' => 'Action URL',
				'desc' => 'This is the Action URL of the form from your autorepsonder code.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'action_url'
			),
			array(
				'name' => 'First Name',
				'desc' => 'This is the name or id of the first name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'fname'
			),
			array(
				'name' => 'Last Name',
				'desc' => 'This is the name or id of the last name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'lname'
			),
			array(
				'name' => 'Email',
				'desc' => 'This is the name or id of the email name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'email'
			),
			array(
				'name' => 'User ID',
				'desc' => 'This is the name or id of the user\'s id field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'user_id'
			),
			array(
				'name' => 'Username',
				'desc' => 'This is the name or id of the username field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'username'
			),
			array(
				'name' => 'Password',
				'desc' => 'This is the name or id of the password field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'password'
			),
			array(
				'name' => 'Phone',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'phone'
			),
			array(
				'name' => 'Birthday',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'birthday'
			),
			array(
				'name' => 'Gender',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'gender'
			),
			array(
				'name' => 'City',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'city'
			),
			array(
				'name' => 'State',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'state'
			),
			array(
				'type' => 'close-left'
			),
/**************************************************************
*          CUSTOM METABOXS AND FIELDS FOR FB REGISTRATION 2
***************************************************************/
			array(
				'name' => '2nd Form Script Setup',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Setup A Second Autoresponder Form - Turn Form On!',
				'desc' => 'IMPORTANT - Turns on the second form.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'second_form'
			),
			array(
				'name' => 'Action URL',
				'desc' => 'This is the Action URL of the form from your autorepsonder code.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'action_url2'
			),
			array(
				'name' => 'First Name',
				'desc' => 'This is the name or id of the first name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'fname2'
			),
			array(
				'name' => 'Last Name',
				'desc' => 'This is the name or id of the last name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'lname2'
			),
			array(
				'name' => 'Email',
				'desc' => 'This is the name or id of the email field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'email2'
			),
			array(
				'name' => 'User ID',
				'desc' => 'This is the name or id of the user\'s id field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'user_id2'
			),
			array(
				'name' => 'Username',
				'desc' => 'This is the name or id of the username field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'username2'
			),
			array(
				'name' => 'Password',
				'desc' => 'This is the name or id of the password field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'password2'
			),
			array(
				'name' => 'Phone',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'phone2'
			),
			array(
				'name' => 'Birthday',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'birthday2'
			),
			array(
				'name' => 'Gender',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'gender2'
			),
			array(
				'name' => 'City',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'city2'
			),
			array(
				'name' => 'State',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'state2'
			),
				array(
					'type' => 'close-left'
			)
		)
	)
);

?>