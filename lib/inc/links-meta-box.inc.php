<?php 
$meta_box = new cmb_Meta_Box( array(
	'id' => 'fanpage_links',
	'title' => 'Redirect Links',
	'pages' => array('diy-link'), // post type
	'context' => 'normal',
	'priority' => 'high',
	'show_names' => true, // Show field names on the left
	'fields' => array(
					array(
					'name' => 'Custom Re-direct',
					'type' => 'section'
					),
					array(
					'type' => 'open'
					),
				array(
					'name' => 'Custom Open Graph (og:) Meta Data',
					'desc' => 'This will allow you to setup custom open graph meta data for your redirected pages.',
					'type' => 'sub_title',
					'id' => VWT_PREFIX . 'main_redirect_title'
				),
				array(
					'name' => 'Open Graph Title',
					'desc' => 'Enter the og:title.  Will show as the Title of your facebook Wall Post.',
					'type' => 'text_medium',
					'id' => VWT_PREFIX . 'fanpage_title'
				),
				array(
					'name' => 'Open Graph Description',
					'desc' => 'Enter the og:description.  Will show as the Description of your facebook Wall Post.',
					'type' => 'text_medium',
					'id' => VWT_PREFIX . 'fanpage_desc'
				),
				array(
					'name' => 'Open Graph Images',
					'desc' => 'Enter the og:images seperated by a comma (,).  Will show as the thumbnail of your facebook Wall Post.',
					'type' => 'text',
					'id' => VWT_PREFIX . 'fanpage_images'
				),
				array(
					'name' => 'Where to Redirect to.',
					'desc' => 'This is where you setup the link you want to redirect to and also indicate if the page is a fan page.',
					'type' => 'sub_title',
					'id' => VWT_PREFIX . 'redirect_title'
				),
				array(
					'name' => 'Send To Fanpage',
					'desc' => 'YES!  This is a fanpage?',
					'type' => 'checkbox',
					'id' => VWT_PREFIX . 'sendto_fanpage_url'
				),
				array(
					'name' => 'Landing Page Url',
					'desc' => 'Enter the url of your fan page OR the url of the site you are linking too.',
					'type' => 'text',
					'id' => VWT_PREFIX . 'post_fanpage_url'
				),
				array(
					'name' => '<span style="color:red">Fan Page Only- Specific Page</span>',
					'desc' => 'Enter the full URL of the page on your site you want to display to your readers.',
					'type' => 'text',
					'id' => VWT_PREFIX . 'link_url'
				),
				array(
				'name' => 'OG:Data Type',
				'desc' => 'What type of content are you linking too?',
				'id' => VWT_PREFIX . 'og_type',
				'type' => 'radio_inline',
				'std' => 'No',
				'options' => array(
						array('name' => ' Post/Page', 'value' => 'No', 'label' => 'article'),
						array('name' => ' Video', 'value' => 'video'),
						array('name' => ' Music', 'value' => 'music'),
						)
				),
				array(
				'type' => 'close'
				),
				array(
				'name' => 'Video Setup',
				'type' => 'section'
				),
				array(
				'type' => 'open'
				),
				array(
					'name' => 'Video Import',
					'desc' => 'This will allow you to import your video into your facebook status or wall post.  Make sure Video is checked above OG:Data Type.',
					'type' => 'title',
					'id' => VWT_PREFIX . 'main_redirect_title'
				),
				array(
					'name' => 'URL',
					'desc' => 'Enter the url to your video file.',
					'type' => 'text_medium',
					'id' => VWT_PREFIX . 'video_url'
				),
				array(
					'name' => 'Width',
					'desc' => 'Enter the width of your video.',
					'type' => 'text_small',
					'id' => VWT_PREFIX . 'video_width'
				),
				array(
					'name' => 'Height',
					'desc' => 'Enter the height of your video.',
					'type' => 'text_small',
					'id' => VWT_PREFIX . 'video_height'
				),
				array(
				'type' => 'close'
				),
			)
		)
	);

?>