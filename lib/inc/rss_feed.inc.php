<?php
global $rss_feed;
global $rss_number;
global $message;
global $important;
global $notify;

if ($rss_feed) {
$rss = fetch_feed($rss_feed); //change RSS feed to your own
	if (!is_wp_error($rss)) { // Checks that the object is created correctly
			// Figure out how many total items there are, and if empty set to 3 as default.
			if (empty($rss_number)){ $rss_number = 3 ;}
			$maxitems = $rss->get_item_quantity($rss_number);
			// Build an array of all the items, starting with element 0 (first element).
			$rss_items = $rss->get_items(0, $maxitems);
	} 
}
if (!empty($rss_items)) {
  if ($important) {
		echo '<div id="smt-important-message" >'.stripslashes($important).'</div>'; }
  if ($message) {
		echo '<div id="smt-message" >'.stripslashes($message).'</div>'; }
  if ($notify) {
		echo '<div>'.stripslashes($notify).'</div>'; }
?>

    <div class="rss-widget">
        <ul>
<?php
    // Loop through each feed item and display each item as a hyperlink.
	  if ($rss_feed) {

    foreach ($rss_items as $item) {
 
?>
            <li><a class="rsswidget" href='<?php echo $item->get_permalink(); ?>'><?php echo $item->get_title(); ?></a> <span class="rss-date"><?php echo $item->get_date('j F Y'); ?></span></li>
<?php } ?>
        </ul>
    </div>
<?php
	} 
}
?>