<?php 
		$meta_box = new cmb_Meta_Box( array(
			'id' => 'fanpage_fields',
			'title' => 'DIY Brand Pages',
			'pages' => array('diy-fanpage'), // post type
			'context' => 'normal',
			'priority' => 'high',
			'show_names' => true, // Show field names on the left
			'fields' => array(
						array(
						'name' => 'Fanpage Setup',
						'type' => 'section'
						),
						array(
						'type' => 'open'
						),
					array(
						'name' => 'Fan Page Setup',
						'desc' => 'This section will allow you to add features to your fan page.',
						'type' => 'title',
						'id' => VWT_PREFIX . 'title_fanpage_setup'
					),
						array(
						'name' => 'Fanpage Url',
						'desc' => 'This is the link to your fanpage.  When this is set, anybody trying to access this page will be redirected to your fanpage.',
						'id' => VWT_PREFIX . 'fanpage_url',
						'type' => 'text'
						),
						array(
						'name' => 'Custom CSS',
						'desc' => 'This css will be put in the head of both the post and the reveal content.)',
						'id' => VWT_PREFIX . 'css',
						'type' => 'textarea_code'
						),
					array(
						'name' => 'Add Navigation Menu',
						'desc' => 'This will add a Fanpage Specific menu to your fanpages.  You can place it above or below the header.',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_nav_menu'
					),
						array(
						'name' => 'Add Fanpage Menu Above Header',
						'desc' => 'This will add a menu Above the Header Image.',
						'id' => VWT_PREFIX . 'menu_above',
						'type' => 'checkbox',	
						),
						array(
						'name' => 'Add Fanpage Menu Below Header',
						'desc' => 'This will add a menu Below the Header Image.',
						'id' => VWT_PREFIX . 'menu_below',
						'type' => 'checkbox',	
						),
					array(
						'name' => 'Header Setup',
						'desc' => 'Add the link to your header image below and set the height.',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_header_setup'
					),
						array(
						'name' => 'Custom Header Image',
						'desc' => 'This css will be put in the head of both the post and the reveal (like gate) content.',
						'id' => VWT_PREFIX . 'header_image',
						'type' => 'text'
						),
						array(
						'name' => 'Custom Header Height',
						'desc' => 'Enter the Height of your header image here in px.)',
						'id' => VWT_PREFIX . 'header_height',
						'type' => 'text_small'
						),
						array(
						'name' => 'Use Header Shadow',
						'desc' => 'This css will be put in the head of both the post and the reveal content.)',
						'id' => VWT_PREFIX . 'header_shadow',
						'type' => 'checkbox',	
						),
					array(
						'name' => 'Layout setup',
						'desc' => 'Choose if you would like to add a sidebar.  Add widgets to the Fanpage Sidebar.',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_layout_setup'
					),
						array(
						'name' => 'Layout',
						'desc' => 'field description (optional)',
						'id' => VWT_PREFIX . 'sidebar_layout',
						'type' => 'radio_inline',
						'std' => 'No',
						'options' => array(
								array('name' => '<span>No Sidebar</span><img src="' .VWT_DIRURL . 'images/content.png" width="136" height="122" /> ', 'value' => 'No', 'label' => '<img src="images/content.png" width="136" height="122" />'),
								array('name' => '<span>Right Sidebar</span><img src="' . VWT_DIRURL . 'images/content-sidebar.png" width="136" height="122" />', 'value' => 'R'),
								array('name' => '<span>Left Sidebar</span><img src="' .VWT_DIRURL . 'images/sidebar-content.png" width="136" height="122" />', 'value' => 'L'),
								)
						),
						array(
						'type' => 'close'
						),
						array(
							'name' => 'Facebook Application Setup',
							'type' => 'section'
						),
						array(
						'type' => 'open'
						),
					array(
						'name' => 'Facebook Application Setup',
						'desc' => 'This is where you associate this page to a facebook application.  This will overide the facebook information on the plugin setup page.',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_fbapp_setup'
					),
						array(
						'name' => 'Facebook Application ID',
						'desc' => '<- This will OVERIDE the default Application ID on the Easy Fanpage Settings page!',
						'id' => VWT_PREFIX . 'fb_app_id',
						'type' => 'text_medium'
						),
						array(
						'name' => 'Facebook Application Secret',
						'desc' => '<- This will OVERIDE the default Application Secret on the Easy Fanpage Settings page!',
						'id' => VWT_PREFIX . 'fb_app_secret',
						'type' => 'text_medium'
						),
						array(
						'type' => 'close'
						),
						array(
							'name' => 'Reveal/Like Gate Page Setup',
							'type' => 'section'
						),
						array(
						'type' => 'open'
						),
					array(
						'name' => 'Hide Content From Non Fans',
						'desc' => 'The Like Gate or Reveal Page is where you setup content depending on the user\'s status on your page',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_like_gate_setup'
					),
						array(
						'name' => 'Like Gate/Reveal Page',
						'desc' => 'Check this box to turn on a "Like Gate" or "Reveal Page"',
						'id' => VWT_PREFIX . 'reveal_checkbox',
						'type' => 'checkbox',	
						),
						array(
						'name' => 'Reveal - Redirect URL',
						'desc' => 'Enter the URL of the page you want to Reveal to your fans OR Enter your content below!',
						'id' => VWT_PREFIX . 'redirect_url',
						'type' => 'text_medium'
						),
						array(
						'name' => 'Reveal Content',
						'desc' => 'Put the content you want to Reveal to those who like your page here.  If there is a url in the Reveal -Redirect URL above, then this will not be shown!',
						'id' => VWT_PREFIX . 'reveal_content',
						'type' => 'wysiwyg'
						),
						array(
						'type' => 'close'
						),
						array(
							'name' => 'Plugin Credits',
							'type' => 'section'
						),
						array(
						'type' => 'open'
						),
						array(
						'name' => 'Disable '.VWT_PLUGIN_NAME.' Credit Link',
						'desc' => 'Check this box if you DON\'T want to display the credit link.',
						'id' => VWT_PREFIX . 'disable_credit',
						'type' => 'checkbox',	
						),
						array(
						'type' => 'close'
						),
					)
				) 
			);
			add_filter( "manage_diy-fanpage_posts_columns", function() {							$cols = array(
				'cb'       => '<input type="checkbox" />',
				'title'       => __( 'Title'),
				'fanpageUrl'      => __( 'Canvas Page Tab Url',      'trans' ),
				'likeGate'       => __( 'Like Gate Redirects to:',  'trans'),
				'date'      => __( 'Date',      'trans' ),
				);
				return $cols;
				} );

			add_action( "manage_diy-fanpage_posts_custom_column", function() {
				global $post;
				switch ( $column ) {
				case "fanpageUrl":
				$fanpageUrl = get_permalink( $post_id );
				echo "<a href='$fanpageUrl'>$fanpageUrl</a>";
				break;
				case "likeGate":
				$redirectUrl = get_post_meta( $post_id,VWT_PREFIX . 'redirect_url' );
				echo "<a href='$redirectUrl[0]'>$redirectUrl[0]</a>";
				break;
					}
				}, 10, 2 );

			add_action('template_redirect', function() {
				if(get_post_type() == 'diy-fanpage') : global $wp_query, $post, $posts;
					include('lib/templates/single-diy-fanpage.php');
					exit; 
					endif;
				});

?>