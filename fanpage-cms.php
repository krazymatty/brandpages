<?php 
/**
Plugin Name: Fampage CMS
Plugin URI: https://philmatthews.net/
Description: Custom Facebook Fan Page CMS
Version: 1.0.0
Author: Philip
Author URI: https://philmatthews.net/
*/

if( !defined( 'VWT_PLUGINURL' ) ) define( 'VWT_PLUGINURL', plugin_dir_url(__FILE__) );

// Load all library files used by this plugin
$libs = glob(dirname(__FILE__).'/lib/*.php');
$classes = glob(dirname(__FILE__).'/classes/*.php');
$dir = (dirname(__FILE__));
$libs = str_replace($dir .'/', '', $libs);
$classes = str_replace($dir .'/', '', $classes);

foreach ($libs as $lib){
				require_once $lib;
				//echo $lib .'<br />';
}
foreach ($classes as $class) {
				require_once $class;
				//echo $class . '<br />';
}
//exit;
/*include('lib/constants.php');
include('classes/VWT_RegisterPlugin.php');
include('classes/Custom_Post_Type.php');
include('classes/Dynamic_Meta_Box.php');
include('classes/Custom_Meta_Boxes.php');*/

class VWT_Plugin {
	public $options = array();
	public $options_slug = 'diy-fanpage';
	// Open Construct
	function __construct() {
		$fanpage_dir = get_option(VWT_PREFIX . 'fanpage_dir');
		$fanpage_dir_register = get_option(VWT_PREFIX . 'fanpage_dir_register');
		$fanpage_dir_redirect = get_option(VWT_PREFIX . 'fanpage_dir_redirect');
		// If they have not been setup then we will default to these.
		if (empty($fanpage_dir)) {
			update_option(VWT_PREFIX . 'fanpage_dir', 'diy-fanpage');
			$fanpage_dir = 'diy-fanpage'; }
		if (empty($fanpage_dir_register)) {
			update_option(VWT_PREFIX . 'fanpage_dir_register', 'diy-register');
			$fanpage_dir_register = 'diy-register'; }
		if (empty($fanpage_dir_redirect)) {
			update_option(VWT_PREFIX . 'fanpage_dir_redirect', 'diy-link'); 
			$fanpage_dir_redirect = 'diy-link'; }
		if( !defined( 'FANPAGE_DIR' ) ) define( 'FANPAGE_DIR', $fanpage_dir );
		if( !defined( 'FANPAGE_DIR_REGISTER' ) ) define( 'FANPAGE_DIR_REGISTER', $fanpage_dir_register );
		if( !defined( 'FANPAGE_DIR_REDIRECT' ) ) define( 'FANPAGE_DIR_REDIRECT', $fanpage_dir_redirect );
		/**************************************************************
		*     CHECK FOR PLUGIN UPDATES
		***************************************************************/
		$post_url='https://diybrandpages.com/diy-support/update-status.php';
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $post_url);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$str = curl_exec($curl_handle);
		curl_close($curl_handle);
		parse_str($str);
		if (VWT_VERSION != $version) {
		  add_action('admin_notices', function() { echo '<div class="error" style="width:660px; background-color: #E5F5FF; border-color: #80CCFF;"><p>There is an Update to the <a href="https://diybrandpages.com/members/download/" target="_blank" style="color:#3366FF">'.VWT_PLUGIN_NAME.' Plugin.</a></p></div>';} );
		}
		if ('true' == $dashboard)  {
			if( !defined( 'RSS_TITLE' ) ) define('RSS_TITLE',  $rss_title);
			if( !defined( 'RSS_FEED' ) ) define('RSS_FEED',  $rss_feed);
			if( !defined( 'RSS_NUMBER' ) ) define('RSS_NUMBER',  $rss_number);
			if( !defined( 'RSS_MESSAGE' ) ) define('RSS_MESSAGE',  $message);
			if( !defined( 'RSS_IMPORTANT' ) ) define('RSS_IMPORTANT',  $important);
			if( !defined( 'RSS_NOTIFY' ) ) define('RSS_NOTIFY',  $notify);
			$this->_smtfeed_widget();
		  }
		$pstatus = new VWT_RegisterPlugin();
		$pstatus->checkactivation('https://diybrandpages.com/diy-support/verify.php');
		
		$this->_add_hooks();

	} 
	// Close Construct
	
	private function _facebooksdk(){
		add_action('wp_head', function() { 
		echo '<div id="fb-root"></div><script type="text/javascript">
		  jQuery(document).ready(function() {
			jQuery.ajaxSetup({ cache: true });
			jQuery.getScript("//connect.facebook.net/en_UK/all.js", function(){
			  FB.init({
				appId       : "'. get_option( VWT_PREFIX.'fb_app_id' ) .'",
				channelUrl  : "'. VWT_DIRURL .'inc/channel.html",
				cookie      : true,
				status      : true,
				xfbml       : true
			  });';
			  $meta_data = get_post_meta(get_the_ID() );

		  if ($meta_data[VWT_PREFIX.'lo_contest'][0]) {
			  $redirect_url = $meta_data[VWT_PREFIX.'lo_redirect_url'][0];
			  $email_list = $meta_data[VWT_PREFIX.'lo_email_list'][0];
			  }
		  if ($meta_data[VWT_PREFIX.'login_checkbox'][0]) {
			  $redirect_url = $meta_data[VWT_PREFIX.'login_redirect_url'][0];
			  $email_list = $meta_data[VWT_PREFIX.'email_list'][0];
		  echo '
			FB.getLoginStatus(function(r){ if(r.status === "connected"){ window.location.href = "'.$redirect_url.'"; } 
			});'; 
		}
			echo '
			  FB.Canvas.setAutoGrow();
			});
		  });'.PHP_EOL.'</script>'.PHP_EOL.'';		  
				
			echo '<script type="text/javascript">
			  function login(){
				FB.getLoginStatus(function(r){ //check if user already authorized the app
					if(r.status === "connected"){
						  window.location.href = "'.VWT_PLUGINURL.'fbconnect.php?url='.$redirect_url.'&list='.$email_list.'&pid='.get_the_ID().'";
					}else{
					  FB.login(function(response) { // opens the login dialog
							  if(response.authResponse) { // check if user authorized the app
							//if (response.perms) {
								  window.location.href = "'.VWT_PLUGINURL.'fbconnect.php?url='.$redirect_url.'&list='.$email_list.'&pid='.get_the_ID().'";
						  } else {
							// user is not logged in
						  }
					},{scope:"email,user_location,user_website,user_birthday"}); //permission required by the app
					}
				}); 
			  }'.PHP_EOL.'</script>'.PHP_EOL.'';
			}); 
		  }
		
	
	private function _smtfeed_widget(){
	  add_action('wp_dashboard_setup', function() { wp_add_dashboard_widget( 'smtfeed_widget', RSS_TITLE, function(){
		  $rss_title = RSS_TITLE;
		  $rss_feed = RSS_FEED;
		  $rss_number = RSS_NUMBER;
		  $message = RSS_MESSAGE;
		  $important = RSS_IMPORTANT;
		  $notify = RSS_NOTIFY;
		  
		  if ($rss_feed) {
		  $rss = fetch_feed($rss_feed); //change RSS feed to your own
			  if (!is_wp_error($rss)) { // Checks that the object is created correctly
					  // Figure out how many total items there are, and if empty set to 3 as default.
					  if (empty($rss_number)){ $rss_number = 3 ;}
					  $maxitems = $rss->get_item_quantity($rss_number);
					  // Build an array of all the items, starting with element 0 (first element).
					  $rss_items = $rss->get_items(0, $maxitems);
			  } 
		  }
		  if (!empty($rss_items)) {
			if ($important) {
				  echo '<div id="smt-important-message" >'.stripslashes($important).'</div>'; }
			if ($message) {
				  echo '<div id="smt-message" >'.stripslashes($message).'</div>'; }
			if ($notify) {
				  echo '<div>'.stripslashes($notify).'</div>'; }
		  ?>
		  
			  <div class="rss-widget">
				  <ul>
		  <?php
			  // Loop through each feed item and display each item as a hyperlink.
				if ($rss_feed) {
		  
			  foreach ($rss_items as $item) {
		   
		  ?>
					  <li><a class="rsswidget" href='<?php echo $item->get_permalink(); ?>'><?php echo $item->get_title(); ?></a> <span class="rss-date"><?php echo $item->get_date('j F Y'); ?></span></li>
		  <?php } ?>
				  </ul>
			  </div>
		  <?php
			  } 
		  }
			echo '<style type="text/css">
			#smt-message { background: #E5F5FF; border: 1px solid #80CCFF; padding:5px; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; margin-bottom:10px; }
			#smt-important-message { background: #FFEBE8; border: 1px solid #CC0000; padding:5px; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; margin-bottom:10px; }
			#smtfeed_widget a:hover, #smtfeed_widget a:active, #smtfeed_widget a:focus {color: #003366;}
			#smtfeed_widget .handlediv {display:none;}
			</style>';
			} );
		  } );
		}
	private function _add_hooks() {
		$this->_facebooksdk();	
		$this->_scripts();	
		$this->_styles();	
		$this->_options(); 
		add_action('admin_menu', array($this, '_options_page')); 
		}
		
	private function _scripts(){
				add_action( 'admin_init', function() {			
				wp_enqueue_script("rm_script", VWT_PLUGINURL ."js/rm_script.js", false, "1.0");
		  } );
		}

	private function _styles(){
				add_action( 'admin_init', function() {			
				wp_enqueue_style('options', VWT_PLUGINURL .'css/options.css', false, '1.0', 'all');
				wp_enqueue_style('meta-css', VWT_PLUGINURL .'css/meta-css.css', false, '1.0', 'all'); 
		  } );
		}

	private function _options() {
		require('lib/inc/options.inc.php');
					$this->options = $options;
					//$this->status();
					$this->_registered();
		}
	
	public function _options_page() {
		$options = $this->options;
		$options_slug = $this->options_slug;
		global $wpdb;
		if ( $_GET['page'] == $options_slug) {
		 
			if ( 'save' == $_REQUEST['action'] ) {
		 
				foreach ($options as $value) {
				update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }
		 
				foreach ($options as $value) {
					if( isset( $_REQUEST[ $value['id'] ] ) ) { 
					update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); 
					} else { 
					delete_option( $value['id'] ); } 
				}

				header('Location: admin.php?page='.$options_slug.'&saved=true');
				die;
			}
		
			if( 'reset' == $_REQUEST['action'] ) {
			
				foreach ($options as $value) {
					delete_option( $value['id'] ); }
			 
				header('Location: admin.php?page='.$options_slug.'&reset=true');
				die; 
			}
		}
	}

	private function status() {
		if (VWT_VALID_KEY && VWT_VALID_URL) {
		  if ('no-key' == VWT_PSTATUS) {
			  $this->_notregistered();
		  } else {
			  $this->_registered(); }
		} else { 		
		if ( $_GET['page'] == $this->options_slug) {
				if (VWT_VALID_KEY && !VWT_VALID_URL) { 
					add_action('admin_notices', function(){	echo '<div class="error" style="width:660px"><p>NOT Registered - You must register '.$_SERVER['SERVER_NAME'].' in the members area of <a href="https://diybrandpages.com/members/activation/" target="_blank">DIY Brand Pages</a> first.</p></div>';} );
					}
				if (VWT_VALID_URL && !VWT_VALID_KEY) {
					add_action('admin_notices', function(){	echo '<div class="error" style="width:660px"><p>NOT Registered - The License Key below is not valid.</p></div>';} );
					}
				if (!VWT_VALID_URL && !VWT_VALID_KEY) {
					add_action('admin_notices', function(){	echo '<div class="error" style="width:700px"><p>NOT Registered - You must Registered the plugin in the members area of <a href="https://diybrandpages.com/members/activation/" target="_blank">DIY Brand Pages</a> before you can register it here.</a></p></div>';} );
					}
				}
			$this->_notregistered();
			}
		}
	public function _notregistered(){
		add_action('admin_menu', array(&$this, '_notactivated_menu')); 
		}

	public function _notactivated_menu(){
	add_menu_page(VWT_PLUGIN_NAME, 'DIY Brand Pages', 'administrator', 'diy-fanpage', array(&$this, 'settings_page'), VWT_PLUGINURL .'images/icon.png', '24');
		}
		
	private function _registered() {
		add_action('admin_menu', array($this, '_activated_menu'));
		$this->diy_fanpages();
		$this->diy_regsiter();
		$this->diy_links();
		$this->_templates();
		$this->_diy_shortcodes();
		$this->_diy_functions();
		}

	public function _activated_menu(){
		add_menu_page(VWT_PLUGIN_NAME, 'DIY Brand Pages', 'administrator', 'diy-fanpage', array(&$this, 'settings_page'), VWT_PLUGINURL .'images/icon.png', '24');
		add_submenu_page('diy-fanpage', 'Add New Fan Page', 'Add New Fan Page<hr style="color:#fff; background-color:#fff;" />', 'administrator', 'post-new.php?post_type=diy-fanpage'); 
		add_submenu_page('diy-fanpage', 'Registration Form', '<span style="color:#3366FF;">All Facebook Forms</span>', 'administrator', 'edit.php?post_type=diy-register'); 
		add_submenu_page('diy-fanpage', 'Add New Registration Form', '<span style="color:#3366FF;">Add New FB Form</span><hr style="color:#fff; background-color:#fff;" />', 'administrator', 'post-new.php?post_type=diy-register');
		add_submenu_page('diy-fanpage', 'Redirect Link', '<span style="color:#1A53FF;">All Redirect Links</span>', 'administrator', 'edit.php?post_type=diy-link'); 
		add_submenu_page('diy-fanpage', 'Add New Redirect Link', '<span style="color:#1A53FF;">Add New Link</span><hr style="color:#fff; background-color:#fff;" />', 'administrator', 'post-new.php?post_type=diy-link');
		add_submenu_page('diy-fanpage', 'Fanpage Settings', '<span style="color:#35AFEE;"><strong><em>Setup Page</em></strong></span>', 'administrator', 'diy-fanpage', array(&$this, 'settings_page'), '999');
	
		}
	private function _diy_shortcodes() {
	  function facebook_registration_form($atts){  
		  extract(shortcode_atts( array('url' => 'url', 'width' => 'width'), $atts)); 
		  if (empty($width)) { $width = '520';}
		  $post_id = get_the_ID(); 
		  $return = '<!--Facebook Registration Plugin--><fb:registration fields="[{\'name\':\'name\'},{\'name\':\'first_name\'},{\'name\':\'last_name\'},{\'name\':\'email\'},{\'name\':\'postid\', \'description\':\'PostID\', \'type\':\'hidden\', \'default\':\''.$post_id.'\'}]"';  
		  /*$return = '<!--Facebook Registration Plugin--><fb:registration fields="[{\'name\':\'name\'},{\'name\':\'first_name\'},{\'name\':\'last_name\'},{\'name\':\'email\'},{\'name\':\'phone\', \'description\':\'Phone\', \'type\':\'text\'},{\'name\':\'password\'},{\'name\':\'postid\', \'description\':\'PostID\', \'type\':\'hidden\', \'default\':\''.$post_id.'\'},{\'name\':\'location\'}]"';*/  
		  $return .= 'redirect-uri="'.$url.'" width="'.$width.'"></fb:registration>';  
		
		  return $return;   
	  }  
	  add_shortcode('facebookform', 'facebook_registration_form');  
	  
	  function facebook_login_button($atts){  
		  extract(shortcode_atts( array('button_url' => 'button_url'), $atts)); 
		  $post_id = get_the_ID(); 
		  $return = '<!--Facebook Login Button--><a href="#" onclick="login();"><img src="'.$button_url.'" alt="" /></a>';  
		
		  return $return;   
	  }  
	  add_shortcode('facebooklogin', 'facebook_login_button');  
	  
 	  function limited_offer_contest($atts, $content){  
		  extract(shortcode_atts( array('button_url' => 'button_url', 'title' => 'title', 'bottom_message' => 'bottom_message'), $atts));
		  
		  $post_id = get_the_ID(); 
		  $lo_offers_received = get_post_meta($post_id, VWT_PREFIX .'lo_offers_recieved', true);  // Check if there are remaing conversions.
		  $lo_conversions = get_post_meta($post_id, VWT_PREFIX .'lo_number_of_offers', true);
		  $lo_remaining_conversions = ($lo_conversions - $lo_offers_received);
		  if (0 === $lo_remaining_conversions) {
		  $expired_content = get_post_meta($post_id, VWT_PREFIX .'lo_expired_content', true);  // Get the post meta data of the referring post.
			  // Display the "contest has ended" content
			  $return = 
				  '<div class="lo_expired_content">'.$expired_content.'</div>';
		  } else {
		  	//if (empty($lo_offers_received)) { $lo_remaining_conversions = $lo_conversions;}
			  $return = 
				  '<div class="lo_counter">
				  	<h1>'.$title.'</h1>
				      <p>'.$content.'</p>
					  <div class="lo_count">'.$lo_remaining_conversions.'</div>
					  <div class="lo_count_bottom">'.$bottom_message.'</div>
					  <div class="lo_button"><!--Facebook Login Button--><a href="#" onclick="login();"><img src="'.$button_url.'" alt="" /></a></div>
				  </div>';  
			  }
		
		  return $return;   
	  }  
	  add_shortcode('lo_contest', 'limited_offer_contest');  

	  function facebook_comments_form($atts){  
		  extract(shortcode_atts( array('posts' => 'posts', 'width' => 'width'), $atts));
		  $post_url = get_permalink($post->ID); 
		  if (empty($width)) { $width = '520';} 
		  if (empty($posts)) { $posts = '2';} 
		  $return = '<!--Facebook Comments-->
	  <div class="fb-comments" data-href="'.$post_url.'" data-num-posts="'.$posts.'" data-width="'.$width.'"></div>';  
		
		  return $return;   
	  }  
	  add_shortcode('facebookcomments', 'facebook_comments_form');  
	  
	  function tinyplugin_add_button($buttons)
	  {
		  array_push($buttons, "separator", "facebookform",$buttons, "separator", "facebooklogin",$buttons, "separator", "facebookcomments",$buttons, "separator", "lo_contest");
		  return $buttons;
	  }
	   
	  function tinyplugin_register($plugin_array)
	  {
		  $sc_url = VWT_PLUGINURL ."/js/shortcodes.js";
	   
		  $plugin_array["facebookform"] = $sc_url;
		  $plugin_array["facebooklogin"] = $sc_url;
		  $plugin_array["facebookcomments"] = $sc_url;
		  $plugin_array["lo_contest"] = $sc_url;
		  return $plugin_array;
	  }
	  add_filter('mce_external_plugins', "tinyplugin_register");
	  add_filter('mce_buttons', 'tinyplugin_add_button', 0);
			}
	
	private function _diy_functions() {
		add_action( 'wp_loaded', function() {	
		register_sidebar(array('name'=>'Fanpage', 'id'=>'diy-sidebar', 'before_title'=>'<h2>', 'after_title'=>'</h2>'));
		  });
		global $wp_rewrite;
		$link_status = get_option(VWT_PREFIX . 'permalink_update');
		if (!$link_status) { 
		//flush_rewrite_rules();
		//update_option(VWT_PREFIX . 'permalink_update', true);
		} 
			}		
	
	private function diy_regsiter() {
		add_action('admin_init', function() { remove_post_type_support( 'diy-register', 'editor' );});
		$register = new Custom_Post_Type( 'Facebook Form', 'diy-register', array( 'rewrite' => array( 'slug' => FANPAGE_DIR_REGISTER), 'show_in_menu' => false, 'menu_position', '25' ));
$meta_box = new cmb_Meta_Box( array(
		'id' => 'fb_app_setup',
		'title' => 'Facebook Application Setup',
		'render_meta_box_content',
		'pages' => array('diy-register'), // post type
		'context' => 'normal',
		'priority' => 'high',
		'show_names' => true, // Show field names on the left
		'show_ui' => true,
		'fields' => array(
		// FACEBOOK APP SETUP
			array(
				'name' => 'Description',
				'desc' => 'Write a quick description on where this data is being stored.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'location_desc'
			),
			array(
				'name' => 'Facebook Application Setup',
				'type' => 'section'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Facebook Application Setup',
				'desc' => 'This is where you associate this page to a facebook application.  This will overide the facebook information on the plugin setup page.',
				'type' => 'sub_title',
				'id' => VWT_PREFIX . 'subtitle_fbapp_setup'
			),
			array(
				'name' => 'Facebook Application ID',
				'desc' => '<- This will OVERRIDE the default Application ID on the Setup Page!',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'app_id'
			),
			array(
				'name' => 'Facebook Application Secret',
				'desc' => '<- This will OVERRIDE the default Application Secret on the Setup Page!',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'app_secret'
			),
			array(
				'type' => 'close'
			),
			// MAILCHIMP INTEGRATION
			array(
				'name' => 'Mailchimp Integration',
				'type' => 'section'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Turn On Mailchimp Integration',
				'desc' => 'Check to "Turn On" Mailchimp!',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_on'
			),
			array(
				'name' => 'Mailchimp API Key',
				'desc' => 'Your mailchimp API key.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'mc_api'
			),
			array(
				'name' => 'Mailchimp Data Server',
				'desc' => 'Your data server looks like this: us4 or us2',
				'type' => 'text_small',
				'id' => VWT_PREFIX . 'mc_data_server'
			),
			array(
				'name' => 'List ID',
				'desc' => 'Your list id.',
				'type' => 'text_small',
				'id' => VWT_PREFIX . 'mc_list_id'
			),
			array(
				'name' => 'Double Opt-in',
				'desc' => 'Check to send your prospet a confirmation email.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_double_opt'
			),
			array(
				'name' => 'Overwrite Existing Contact',
				'desc' => 'This will overwrite a contact that already exist.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_overwrite'
			),
			array(
				'name' => 'Update Contact',
				'desc' => 'This will update the contact.  For example subscriber date.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_update'
			),
			array(
				'name' => 'Send Welcome Email',
				'desc' => 'This will send the Welcome Email that is set for the campaign.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mc_send_welcome'
			),
			array(
					'type' => 'close'
			),
			// MY Mail INTEGRATION
			array(
				'name' => 'MyMail Integration',
				'type' => 'section'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Turn On MyMail Integration',
				'desc' => 'Check to "Turn On" MyMail!',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'mm_subscribe'
			),
			array(
				'name' => 'MyMail List ID',
				'desc' => 'Your MyMail List ID.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'mm_list_id'
			),
			array(
					'type' => 'close'
			),
			/*array(
				'name' => 'Digital Access Pass (DAP) SETUP',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'DAP Auto Login',
				'desc' => 'Turns on the DAP Auto Login',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'auto_login'
			),
			array(
					'type' => 'close-left'
			),*/
			// REDIRECT LANDING PAGE
			array(
				'name' => 'Redirect to Fan Page or Thank You/Landing Page',
				'type' => 'section'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Form Submission Redirect',
				'desc' => 'This is the url the user will be redirected to after they submit the form.',
				'type' => 'sub_title',
				'id' => VWT_PREFIX . 'subtitle_form_redirect_setup'
			),
			array(
				'name' => 'Are You Redirecting To A Fanpage?',
				'desc' => 'YES!  This is a Fanpage redirect.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'fb_redirect'
			),
			array(
				'name' => 'Fan Page Or Thank You Page URL',
				'desc' => 'Enter URL you would like to redirect to.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'redirect'
			),
			array(
				'name' => '<span style="color:red">Fan Page Only- Specific Page</span>',
				'desc' => 'Enter the URL to the page you would like to display.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'fb_redirect_link'
			),
			array(
					'type' => 'close'
			),
/**************************************************************
*         CUSTOM METABOXS AND FIELDS FOR FB REGISTRATION
***************************************************************/
			array(
				'name' => 'Custom Form Submission Setup',
				'desc' => 'This script will allow you to use facebooks registration plugin.  When the user submits the form it will be sent to the script you setup and the information sent to your autoresponders or membership databases.',
				'type' => 'divider',
				'id' => VWT_PREFIX . 'fb_title_register_plugin'
			),
			array(
				'name' => 'Form Script Setup',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Autoresponder/Newsletter Setup',
				'desc' => 'This is where you merge the facebook registration setup with your autoresponder or newsletter form submission code.',
				'type' => 'sub_title',
				'id' => VWT_PREFIX . 'subtitle_autoresponder_setup'
			),
			array(
				'name' => 'Action URL',
				'desc' => 'This is the Action URL of the form from your autorepsonder code.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'action_url'
			),
			array(
				'name' => 'First Name',
				'desc' => 'This is the name or id of the first name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'fname'
			),
			array(
				'name' => 'Last Name',
				'desc' => 'This is the name or id of the last name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'lname'
			),
			array(
				'name' => 'Email',
				'desc' => 'This is the name or id of the email name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'email'
			),
			array(
				'name' => 'User ID',
				'desc' => 'This is the name or id of the user\'s id field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'user_id'
			),
			array(
				'name' => 'Username',
				'desc' => 'This is the name or id of the username field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'username'
			),
			array(
				'name' => 'Password',
				'desc' => 'This is the name or id of the password field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'password'
			),
			array(
				'name' => 'Phone',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'phone'
			),
			array(
				'name' => 'Birthday',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'birthday'
			),
			array(
				'name' => 'Gender',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'gender'
			),
			array(
				'name' => 'City',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'city'
			),
			array(
				'name' => 'State',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'state'
			),
			array(
				'type' => 'close-left'
			),
/**************************************************************
*          CUSTOM METABOXS AND FIELDS FOR FB REGISTRATION 2
***************************************************************/
			array(
				'name' => '2nd Form Script Setup',
				'type' => 'section-left'
			),
			array(
				'type' => 'open'
			),
			array(
				'name' => 'Setup A Second Autoresponder Form - Turn Form On!',
				'desc' => 'IMPORTANT - Turns on the second form.',
				'type' => 'checkbox',
				'id' => VWT_PREFIX . 'second_form'
			),
			array(
				'name' => 'Action URL',
				'desc' => 'This is the Action URL of the form from your autorepsonder code.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'action_url2'
			),
			array(
				'name' => 'First Name',
				'desc' => 'This is the name or id of the first name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'fname2'
			),
			array(
				'name' => 'Last Name',
				'desc' => 'This is the name or id of the last name field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'lname2'
			),
			array(
				'name' => 'Email',
				'desc' => 'This is the name or id of the email field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'email2'
			),
			array(
				'name' => 'User ID',
				'desc' => 'This is the name or id of the user\'s id field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'user_id2'
			),
			array(
				'name' => 'Username',
				'desc' => 'This is the name or id of the username field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'username2'
			),
			array(
				'name' => 'Password',
				'desc' => 'This is the name or id of the password field in your form.',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'password2'
			),
			array(
				'name' => 'Phone',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'phone2'
			),
			array(
				'name' => 'Birthday',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'birthday2'
			),
			array(
				'name' => 'Gender',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'gender2'
			),
			array(
				'name' => 'City',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'city2'
			),
			array(
				'name' => 'State',
				'desc' => '',
				'type' => 'text_medium',
				'id' => VWT_PREFIX . 'state2'
			),
				array(
					'type' => 'close-left'
			)
		)
	)
);
		$form = new Dynamic_Meta_Box('Hidden Fields 1', 'Hidden Fields for First Form ', '_diy1_', 'diy-register');		
		$form2 = new Dynamic_Meta_Box('Hidden Fields 2', 'Hidden Fields for 2nd Form', '_diy2_', 'diy-register');
		
		add_filter( "manage_diy-register_posts_columns", function() {
				$cols = array(
		
				'cb'       => '<input type="checkbox" />',
			'title'       => __( 'Title'),
			'location'       => __( 'Location Description'),
			'formurl'      => __( 'Form Action Url',      'trans' ),
			);
			return $cols;
		} );

		add_action( "manage_diy-register_posts_custom_column", "custom_diy_register_column", 10, 2);
		function custom_diy_register_column($column, $post_id) {
				global $post;
			switch ( $column ) {
				case "location":
					$facebook = get_post_meta( $post_id, VWT_PREFIX . 'location_desc' );
					echo $facebook[0];
					break;
				case "formurl":
					$formurl = get_permalink( $post_id );
					echo "<a href='$formurl'>$formurl</a>";
					break;
			}
		
		}

	}

	private function diy_fanpages() {
		$fanpage = new Custom_Post_Type( 'Fanpage', 'diy-fanpage', array( 'rewrite' => array( 'slug' => FANPAGE_DIR ), 'show_in_menu' => 'diy-fanpage', 'menu_position', '24' ));
		$meta_box = new cmb_Meta_Box( array(
			'id' => 'fanpage_fields',
			'title' => 'DIY Brand Pages',
			'pages' => array('diy-fanpage'), // post type
			'context' => 'normal',
			'priority' => 'high',
			'show_names' => true, // Show field names on the left
			'fields' => array(
						array(
						'name' => 'Fanpage Setup',
						'type' => 'section'
						),
						array(
						'type' => 'open'
						),
						array(
						'name' => 'Fan Page Setup',
						'desc' => 'This section will allow you to add features to your fan page.',
						'type' => 'title',
						'id' => VWT_PREFIX . 'title_fanpage_setup'
						),
						/*array(
						'name' => 'Fanpage Url',
						'desc' => 'This is the link to your fanpage.  When this is set, anybody trying to access this page will be redirected to your fanpage.',
						'id' => VWT_PREFIX . 'fanpage_url',
						'type' => 'text'
						),*/
						array(
						'name' => 'Custom CSS',
						'desc' => 'This css will be put in the head of both the post and the reveal content.)',
						'id' => VWT_PREFIX . 'css',
						'type' => 'textarea_code'
						),
/*						array(
						'name' => 'Style',
						'desc' => 'Pick Your Layout',
						'id' => VWT_PREFIX . 'style',
						'type' => 'select',
						'std' => '---',
						'options' => array(
								array( 'name' => '--Select', 'value' => '', ),
								array( 'name' => 'Box Top', 'value' => 'box-top', ),
								array( 'name' => 'Video', 'value' => 'video', ),
								)
						),
*/						array(
						'name' => 'Add Navigation Menu',
						'desc' => 'This will add a Fanpage Specific menu to your fanpages.  You can place it above or below the header.',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_nav_menu'
						),
						array(
						'name' => 'Add Fanpage Menu Above Header',
						'desc' => 'This will add a menu Above the Header Image.',
						'id' => VWT_PREFIX . 'menu_above',
						'type' => 'checkbox',	
						),
						array(
						'name' => 'Add Fanpage Menu Below Header',
						'desc' => 'This will add a menu Below the Header Image.',
						'id' => VWT_PREFIX . 'menu_below',
						'type' => 'checkbox',	
						),
						array(
						'name' => 'Header Setup',
						'desc' => 'Add the link to your header image below and set the height.',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_header_setup'
						),
						array(
						'name' => 'Custom Header Image',
						'desc' => 'This css will be put in the head of both the post and the reveal (like gate) content.',
						'id' => VWT_PREFIX . 'header_image',
						'type' => 'text'
						),
						array(
						'name' => 'Custom Header Height',
						'desc' => 'Enter the Height of your header image here in px.)',
						'id' => VWT_PREFIX . 'header_height',
						'type' => 'text_small'
						),
						array(
						'name' => 'Use Header Shadow',
						'desc' => 'This css will be put in the head of both the post and the reveal content.)',
						'id' => VWT_PREFIX . 'header_shadow',
						'type' => 'checkbox',	
						),
						array(
						'name' => 'Layout setup',
						'desc' => 'Choose if you would like to add a sidebar.  Add widgets to the Fanpage Sidebar.',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_layout_setup'
						),
						array(
						'name' => 'Layout',
						'desc' => 'field description (optional)',
						'id' => VWT_PREFIX . 'sidebar_layout',
						'type' => 'radio_inline',
						'std' => 'No',
						'options' => array(
								array('name' => '<span>No Sidebar</span><img src="' .VWT_PLUGINURL . 'css/images/content.png" width="136" height="122" /> ', 'value' => 'No', 'label' => '<img src="images/content.png" width="136" height="122" />'),
								array('name' => '<span>Right Sidebar</span><img src="' . VWT_PLUGINURL . 'css/images/content-sidebar.png" width="136" height="122" />', 'value' => 'R'),
								array('name' => '<span>Left Sidebar</span><img src="' .VWT_PLUGINURL . 'css/images/sidebar-content.png" width="136" height="122" />', 'value' => 'L'),
								)
						),
						array(
						'type' => 'close'
						),
						array(
						'name' => 'Facebook Login',
						'type' => 'section'
						),
						array(
						'type' => 'open'
						),
						array(
						'name' => 'Turn On Login',
						'desc' => 'Check this box to turn facebook login.',
						'id' => VWT_PREFIX . 'login_checkbox',
						'type' => 'checkbox',	
						),
						array(
						'name' => 'Login - Redirect URL',
						'desc' => 'Enter the URL of the page you want to redirect to after fans login.',
						'id' => VWT_PREFIX . 'login_redirect_url',
						'type' => 'text_medium'
						),
						array(
						'name' => 'Email List',
						'desc' => 'Enter the name of the list you would like to add your subscribers to.',
						'id' => VWT_PREFIX . 'email_list',
						'type' => 'text_medium'
						),
						array(
						'type' => 'close'
						),
						array(
							'name' => 'Reveal/Like Gate Page Setup',
							'type' => 'section'
						),
						array(
						'type' => 'open'
						),
						array(
						'name' => 'Hide Content From Non Fans',
						'desc' => 'The Like Gate or Reveal Page is where you setup content depending on the user\'s status on your page',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_like_gate_setup'
						),
						array(
						'name' => 'Like Gate/Reveal Page',
						'desc' => 'Check this box to turn on a "Like Gate" or "Reveal Page"',
						'id' => VWT_PREFIX . 'reveal_checkbox',
						'type' => 'checkbox',	
						),
						array(
						'name' => 'Reveal - Redirect URL',
						'desc' => 'Enter the URL of the page you want to Reveal to your fans OR Enter your content below!',
						'id' => VWT_PREFIX . 'redirect_url',
						'type' => 'text_medium'
						),
						array(
						'name' => 'Reveal Content',
						'desc' => 'Put the content you want to Reveal to those who like your page here.  If there is a url in the Reveal -Redirect URL above, then this will not be shown!',
						'id' => VWT_PREFIX . 'reveal_content',
						'type' => 'wysiwyg'
						),
						array(
						'type' => 'close'
						),
						array(
						'name' => 'Count Down Timer Setup',
						'type' => 'section'
						),
						array(
						'type' => 'open'
						),
						array(
						'name' => 'Countdown Timer',
						'desc' => 'Initiate a Count Down Timer. Use this tag in your content: <strong>{COUNTDOWN_TIMER}</strong>',
						'id' => VWT_PREFIX . 'countdown_timer',
						'type' => 'text_datetime_timestamp',
						),
						array(
						'name' => 'Timer - Redirect URL',
						'desc' => 'Enter the URL of the page you want to redirect to aftet your timer has expired.',
						'id' => VWT_PREFIX . 'timer_redirect_url',
						'type' => 'text_medium'
						),
						array(
						'name' => 'Timer Content',
						'desc' => 'Put the content you want to Reveal after the timer has expired. Use this tag in your content: <strong>{COUNTDOWN_TIMER_CONTENT}</strong> ',
						'id' => VWT_PREFIX . 'timer_content',
						'type' => 'wysiwyg'
						),
						array(
						'type' => 'close'
						),
						array(
						'name' => 'Limited Offer - Contest Count Down',
						'type' => 'section'
						),
						array(
						'type' => 'open'
						),
						array(
						'name' => 'Start A Contest',
						'desc' => 'Check this box to turn on a Contest.',
						'id' => VWT_PREFIX . 'lo_contest',
						'type' => 'checkbox',	
						),
						array(
						'name' => 'Number of Offers Available.',
						'desc' => 'Enter the total number of offers for this contest.',
						'id' => VWT_PREFIX . 'lo_number_of_offers',
						'type' => 'text_small'
						),
						array(
						'name' => 'Limited Offer - Redirect URL',
						'desc' => 'Enter the URL of the page you want to send your participants too.',
						'id' => VWT_PREFIX . 'lo_redirect_url',
						'type' => 'text_medium'
						),
						array(
						'name' => 'Contest List',
						'desc' => 'Enter the list name of where you want to store your contest participants.',
						'id' => VWT_PREFIX . 'lo_email_list',
						'type' => 'text_medium'
						),
						array(
						'name' => 'Expired Content',
						'desc' => 'The content you want to Reveal when the counter reaches zero.',
						'id' => VWT_PREFIX . 'lo_expired_content',
						'type' => 'wysiwyg'
						),
						array(
						'type' => 'close'
						),
						array(
							'name' => 'Facebook Application Setup',
							'type' => 'section'
						),
						array(
						'type' => 'open'
						),
						array(
						'name' => 'OverrideFacebook Application Setup',
						'desc' => 'This is where you associate this page to a facebook application.  This will overide the facebook information on the plugin setup page.',
						'type' => 'sub_title',
						'id' => VWT_PREFIX . 'subtitle_fbapp_setup'
						),
						array(
						'name' => 'Facebook Application ID',
						'desc' => '<- This will OVERIDE the default Application ID on the Easy Fanpage Settings page!',
						'id' => VWT_PREFIX . 'fb_app_id',
						'type' => 'text_medium'
						),
						array(
						'name' => 'Facebook Application Secret',
						'desc' => '<- This will OVERIDE the default Application Secret on the Easy Fanpage Settings page!',
						'id' => VWT_PREFIX . 'fb_app_secret',
						'type' => 'text_medium'
						),
						array(
						'type' => 'close'
						),
						array(
							'name' => 'Plugin Credits',
							'type' => 'section'
						),
						array(
						'type' => 'open'
						),
						array(
						'name' => 'Disable '.VWT_PLUGIN_NAME.' Credit Link',
						'desc' => 'Check this box if you DON\'T want to display the credit link.',
						'id' => VWT_PREFIX . 'disable_credit',
						'type' => 'checkbox',	
						),
						array(
						'type' => 'close'
						),
					)
				) 
			);
			add_filter( "manage_diy-fanpage_posts_columns", function() {							$cols = array(
				'cb'       => '<input type="checkbox" />',
				'title'       => __( 'Title'),
				'fanpageUrl'      => __( 'Canvas Page Tab Url',      'trans' ),
				'likeGate'       => __( 'Like Gate Redirects to:',  'trans'),
				'date'      => __( 'Date',      'trans' ),
				);
				return $cols;
				} );

			add_action( "manage_diy-fanpage_posts_custom_column", "custom_diy_fanpage_column", 10, 2 );
			function custom_diy_fanpage_column( $column, $post_id ) {
				global $post;
				switch ( $column ) {
				case "fanpageUrl":
				$fanpageUrl = get_permalink( $post_id );
				echo "<a href='$fanpageUrl'>$fanpageUrl</a>";
				break;
				case "likeGate":
				$redirectUrl = get_post_meta( $post_id, VWT_PREFIX . 'redirect_url' );
				echo "<a href='$redirectUrl[0]'>$redirectUrl[0]</a>";
				break;
				  }
			}

		}
	
	private function diy_links() {
		add_action('admin_init', function() { remove_post_type_support( 'diy-link', 'editor' );});
		$links = new Custom_Post_Type( 'Redirect Link', 'diy-link', array( 'rewrite' => array( 'slug' => FANPAGE_DIR_REDIRECT), 'show_in_menu' => false, 'menu_position', '25' ));
		$meta_box = new cmb_Meta_Box( array(
			'id' => 'fanpage_links',
			'title' => 'Redirect Links',
			'pages' => array('diy-link'), // post type
			'context' => 'normal',
			'priority' => 'high',
			'show_names' => true, // Show field names on the left
			'fields' => array(
				array(
					'name' => 'Custom Re-direct',
					'type' => 'section'
					),
					array(
					'type' => 'open'
					),
				array(
					'name' => 'Custom Open Graph (og:) Meta Data',
					'desc' => 'This will allow you to setup custom open graph meta data for your redirected pages.',
					'type' => 'sub_title',
					'id' => VWT_PREFIX . 'main_redirect_title'
				),
				array(
					'name' => 'Open Graph Title',
					'desc' => 'Enter the og:title.  Will show as the Title of your facebook Wall Post.',
					'type' => 'text_medium',
					'id' => VWT_PREFIX . 'fanpage_title'
				),
				array(
					'name' => 'Open Graph Description',
					'desc' => 'Enter the og:description.  Will show as the Description of your facebook Wall Post.',
					'type' => 'text_medium',
					'id' => VWT_PREFIX . 'fanpage_desc'
				),
				array(
					'name' => 'Open Graph Images',
					'desc' => 'Enter the og:images seperated by a comma (,).  Will show as the thumbnail of your facebook Wall Post.',
					'type' => 'text',
					'id' => VWT_PREFIX . 'fanpage_images'
				),
				array(
					'name' => 'Where to Redirect to.',
					'desc' => 'This is where you setup the link you want to redirect to and also indicate if the page is a fan page.',
					'type' => 'sub_title',
					'id' => VWT_PREFIX . 'redirect_title'
				),
				array(
					'name' => 'Send To Fanpage',
					'desc' => 'YES!  This is a fanpage?',
					'type' => 'checkbox',
					'id' => VWT_PREFIX . 'sendto_fanpage_url'
				),
				array(
					'name' => 'Landing Page Url',
					'desc' => 'Enter the url of your fan page OR the url of the site you are linking too.',
					'type' => 'text',
					'id' => VWT_PREFIX . 'post_fanpage_url'
				),
				array(
					'name' => '<span style="color:red">Fan Page Only- Specific Page</span>',
					'desc' => 'Enter the full URL of the page on your site you want to display to your readers.',
					'type' => 'text',
					'id' => VWT_PREFIX . 'link_url'
				),
				array(
				'name' => 'OG:Data Type',
				'desc' => 'What type of content are you linking too?',
				'id' => VWT_PREFIX . 'og_type',
				'type' => 'radio_inline',
				'std' => 'No',
				'options' => array(
						array('name' => ' Post/Page', 'value' => 'No', 'label' => 'article'),
						array('name' => ' Video', 'value' => 'video'),
						array('name' => ' Music', 'value' => 'music'),
						)
				),
				array(
				'type' => 'close'
				),
				array(
				'name' => 'Video Setup',
				'type' => 'section'
				),
				array(
				'type' => 'open'
				),
				array(
					'name' => 'Video Import',
					'desc' => 'This will allow you to import your video into your facebook status or wall post.  Make sure Video is checked above OG:Data Type.',
					'type' => 'title',
					'id' => VWT_PREFIX . 'main_redirect_title'
				),
				array(
					'name' => 'URL',
					'desc' => 'Enter the url to your video file.',
					'type' => 'text_medium',
					'id' => VWT_PREFIX . 'video_url'
				),
				array(
					'name' => 'Width',
					'desc' => 'Enter the width of your video.',
					'type' => 'text_small',
					'id' => VWT_PREFIX . 'video_width'
				),
				array(
					'name' => 'Height',
					'desc' => 'Enter the height of your video.',
					'type' => 'text_small',
					'id' => VWT_PREFIX . 'video_height'
				),
				array(
				'type' => 'close'
				),
			)
		)
	);
		add_filter( "manage_diy-link_posts_columns", function() {
				$cols = array(
			'cb'       => '<input type="checkbox" />',
			'title'       => __( 'Title'),
			'linkTitle'       => __( 'Link Title'),
			'linkDescription'       => __( 'Link Description'),
			'url'      => __( 'Redirect Url',      'trans' ),
			);
			return $cols;
		} );

		add_action( "manage_diy-link_posts_custom_column", "manage_diy_link_column", 10, 2 );
		function manage_diy_link_column( $column, $post_id ) {
				global $post;
			switch ( $column ) {
				case "linkTitle":
					$facebook = get_post_meta( $post_id, VWT_PREFIX . 'fanpage_title' );
					echo $facebook[0];
					break;
				case "linkDescription":
					$facebook = get_post_meta( $post_id, VWT_PREFIX . 'fanpage_desc' );
					echo $facebook[0];
					break;
				case "url":
					$url = get_permalink( $post_id );
					echo "<a href='$url'>$url</a>";
					break;
					}
				}

		}
	
	public function _templates(){
		add_action('template_redirect', function() {
			global $wp_query, $post, $posts;
			if (get_post_type() == 'diy-fanpage') {
				include('lib/templates/single-diy-fanpage.php');
				exit; 
			} elseif (get_post_type() == 'diy-register') { 
					include('lib/templates/single-diy-register.php');
					exit; 
			} elseif (get_post_type() == 'diy-link') { 
					include('lib/templates/single-diy-link.php');
					exit; 
				}
			});
		}
		
	public function settings_page(){
		$options = $this->options;
		$i = 0;
		$ni = 0;
		 
		if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.VWT_PLUGIN_NAME.' settings saved.</strong></p></div>';
		if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.VWT_PLUGIN_NAME.' settings reset.</strong></p></div>';
		 
		?>
		<div class="wrap rm_wrap">
			<h2><?php echo VWT_PLUGIN_NAME; ?> Settings</h2>
			<div class="rm_opts">
			<form method="post">
			<?php foreach ($options as $value) {
				switch ( $value['type'] ) {
		
				case "section":
			
				$i++;    ?>
				
				<div class="rm_section">
					<div class="rm_title">
						<h3><img src="<?php echo VWT_PLUGINURL;?>images/trans.gif" class="inactive" alt="""><?php echo $value['name']; ?><span style="text-transform:none; margin-left:10px;"><?php echo $value['desc']; ?></span></h3>
						<span class="submit">
						<input name="save<?php echo $i; ?>" class="button-primary" type="submit" value="Save changes" />
						</span>
						<div class="clearfix"></div>
					</div>
				<div class="rm_options">
				<?php break;
				 
				case "open":
				break;
		
				case "title":
				?>
				<p>Fill out the Activation form below to Activate the <?php echo VWT_PLUGIN_NAME;?> plugin. <span style="font-weight:bold; color:#900;">Status:
					<?php if (get_option(VWT_PREFIX . 'status')) { echo get_option(VWT_PREFIX . 'status');} else { echo 'Not Activated';} ?>
					</span></p>
				<p>Version <?php echo VWT_VERSION; ?></p>
				<?php break;
				 
				case 'text':
				?>
				<div class="rm_input rm_text">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id'])  ); } else { echo $value['std']; } ?>" />
					<small><?php echo $value['desc']; ?></small>
					<div class="clearfix"></div>
				</div>
				<?php
				break;
				 
				case 'textarea':
				?>
				<div class="rm_input rm_textarea">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id']) ); } else { echo $value['std']; } ?>
					</textarea>
					<small><?php echo $value['desc']; ?></small>
					<div class="clearfix"></div>
				</div>
				<?php
				break;
				 
				case 'select':
				?>
				<div class="rm_input rm_select">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
						<?php foreach ($value['options'] as $option) { ?>
						<option <?php if (get_option( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option>
						<?php } ?>
					</select>
					<small><?php echo $value['desc']; ?></small>
					<div class="clearfix"></div>
				</div>
				<?php
				break;
				 
				case "checkbox":
				?>
				<div class="rm_input rm_checkbox">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
					<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
					<small><?php echo $value['desc']; ?></small>
					<div class="clearfix"></div>
				</div>
				<?php break; 
				
				case "close":     ?>
				
				</div>
				</div>
				<br />
				<?php break; 
				} 
			} ?>
				
				<input type="hidden" name="action" value="save" />
				</form>
				<form method="post">
					<p class="submit">
						<input name="reset" type="submit" value="Total Reset" />
						<input type="hidden" name="action" value="reset" />
					</p>
				</form>
			</div>
		</div>
	<?php
	}
}

$VWT_Plugin = new	VWT_Plugin();

?>